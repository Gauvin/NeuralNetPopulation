
import sys
import numpy as np

from keras.applications.vgg19 import VGG19
from keras.optimizers import SGD, Adam, RMSprop
from sklearn.preprocessing import MinMaxScaler

import os
home = os.environ['HOME']
sys.path.append(f"{home}/Projects/NeuralNetPopulation/Python")

from dataGeneration.dataGeneratorFactory import DataGeneratorFactory
from dataGeneration.rawDataInput import RawDataInput

from models.convNetParams import ConvNetParams
from models.mdlFit import MdlFit
from models.mdlFitExisting import MdlFitExisting
from models.mdlEval import MdlEval
from models.mdlLayerInspection import MdlLayerInspection

from utils.pathBuilder import PathBuilder


np.random.seed(1)


rootDir = f'{home}/Projects/NeuralNetPopulation'
provider = 'Mapbox'
gridSize = 100
resp="v_CA16_406"
penultimateNonLin ='elu'
finalNonLin='linear' #final should be in 0,1 (the train data definitely are)
optimizerClass = Adam
lrInit=10**-2
numEpochs=100
finalFcUnits = 2**8 #try something smaller than the initial 16 to reduce the computational burden and larger to avoid saturating loss
lossFct='mean_squared_error' # mean_absolute_error
usePreFilter = True


refitExistingModel = True
numIncrementalEpochs = 38

def main():
    myPathBuilder = PathBuilder(rootDir,
                                provider,
                                gridSize)

    myRawDataInput = RawDataInput(myPathBuilder,
                                  idGrid='gridId',
                                  provider=provider,
                                  gridSize=gridSize,
                                  resp=resp,
                                  ubNumTotal=np.inf)  # consider all the data

    myConvNetParams = ConvNetParams(provider=provider,
                                    gridSize=gridSize,
                                    resp=myRawDataInput.resp,
                                    dim=myRawDataInput.dim,
                                    numImgs=myRawDataInput.numImgs,
                                    penultimateNonLin =penultimateNonLin,
                                    finalNonLin=finalNonLin,
                                    finalFcUnits=finalFcUnits,
                                    lr=lrInit,
                                    numEpochs=numEpochs,
                                    batchSize=2 ** 5,
                                    l1Coeff=0,
                                    dropoutRate=0,
                                    convNetClass=VGG19,
                                    usePreFilter=usePreFilter,
                                    lossFct='mean_squared_error',
                                    optimizerClass=optimizerClass,
                                    scalerClass=MinMaxScaler,
                                    extraStrId='',
                                    shuffle=False,
                                    verbose=True)

    myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                  myConvNetParams,
                                                  myPathBuilder)

    #Fit
    myMdlFit = MdlFit(myDatageneratorFactory,
                      myConvNetParams,
                      myPathBuilder)


    #Refit the existing model if desired + eval that final model erformance
    if refitExistingModel:
        myMdlFitRefit = MdlFitExisting(myMdlFit,
                                       numIncrementalEpochs=numIncrementalEpochs)
        myMdlFit = myMdlFitRefit

    #Eval predictions + errors
    myMdlEval =  MdlEval( myMdlFit,
                          myConvNetParams,
                          myPathBuilder,
                          myDatageneratorFactory)

    # Plot the distribution -- should call predict by default if the predictions have not already been computed
    myMdlEval.plotErrorPredDistribution()


    #Eval the conv layers with tsne
    myMdlLayerInspection = MdlLayerInspection(myDatageneratorFactory,
                                              myConvNetParams,
                                              myPathBuilder,
                                              myMdlFit)

    #Distribution of the weights for the last feature maps
    myMdlLayerInspection.plotTsneRepLastLayerAll()

    #Lower dimensional
    myMdlLayerInspection.plotDistributionAllFCDim()


if __name__ == '__main__':
    main()
