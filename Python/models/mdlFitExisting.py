from keras.layers import Input, Dense, Flatten, Dropout, MaxPooling2D, Conv2D
from keras.models import Model
from keras.models import load_model
from keras.regularizers import l1
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.optimizers.schedules import ExponentialDecay
from keras.initializers import Constant
import keras.backend as K

import matplotlib.pyplot as plt
import seaborn as sns

import warnings
import sys
import os
import pandas as pd
import numpy as np
import re

import os
home = os.environ['HOME']
sys.path.append(f"{home}/Projects/NeuralNetPopulation/Python")

from dataGeneration.dataGeneratorFactory import DataGeneratorFactory
from dataGeneration.rawDataInput import RawDataInput

from models.convNetParams import ConvNetParams
from models.mdlFit import MdlFit
from utils.pathBuilder import PathBuilder

class MdlFitExisting(object):

    def __init__(self,
                 mdlFit: MdlFit,
                 numIncrementalEpochs: int):

        '''Basic constructor takes an existing model as argument
        '''

        self.mdlFit=mdlFit


        self.pathBuilder= self.mdlFit.pathBuilder
        self.convNetParams= self.mdlFit.convNetParams
        self.dataGeneratorFactory = self.mdlFit.dataGeneratorFactory

        self.numIncrementalEpochs = numIncrementalEpochs



        self.convNetMdlCustom = self._fitMdl()

        try:
            self._plotHistory()
        except Exception as e:
            print(f"Error printing in MdlFit - {e}")

        self.numParams = self.mdlFit.numParams




    def _plotHistory(self):



        print("Plotting the history")

        historyPath = self.pathBuilder.historyOutputPath / f"history_{self.mdlId}.csv"
        figureHistoryPath = self.pathBuilder.figuresOutputPath / 'LossFctHistory' / f'mdlHistory_{self.mdlId}.png'

        dfHist = pd.read_csv(str(historyPath))

        #Data manips
        dfHist.rename( columns={dfHist.columns[0] : "epoch"}, inplace=True)
        dfHist['epoch'] = np.arange(dfHist.shape[0])
        dfMelted=dfHist.loc[:,['epoch',  'loss',  'val_loss' ]].melt(id_vars='epoch')

        #Get the quantiles and truncate to avoid excessively large values + compute log for better visualization
        quant = np.quantile(dfMelted['value'], q=0.95)
        idxToTrunc = dfMelted['value'] > quant
        dfMelted.loc[idxToTrunc, 'value'] = quant
        dfMelted['logloss'] = dfMelted['value'].apply(lambda x: np.log(x))

        #Now plot it
        f = plt.figure(figsize=(7, 7))
        sns.lineplot(x='epoch', y='logloss', hue='variable', data=dfMelted )
        plt.title(
            f"Objection function history - {self.convNetParams.resp} \n" \
                f"Loss (5% largest outliers truncated - {self.convNetParams.numImgs} images)\n" \
                f"{self.convNetParams.numEpochs} iterations + additional epochs"
        )
        f.savefig(figureHistoryPath)



    def _getOptimizer(self):

        '''Should refactor this fct into a mdlFitDefault class from which MdlFitExisting and MdlFit could inherit or be composed
        '''

        lr_schedule = ExponentialDecay(
            self.convNetParams.lr, #needs a relatively high value, otherwise will surely stagnate very fast
            decay_steps=10**5, #decay each number of steps
            decay_rate=0.96, #quickly decaying
            staircase=True) # strictly monotonic decreasing if False

        optInst = self.convNetParams.optimizerClass(learning_rate=lr_schedule)


        return optInst


    def _getCallBacks(self):

        # Early stopping for the evaluation loss
        es1 = EarlyStopping(monitor='val_loss',
                            verbose=1,
                            min_delta=10 ** -4,
                            patience=int(self.numIncrementalEpochs*0.75)) #Very patient: allow for 75% of full iterations

        checkpointLoss = ModelCheckpoint(str( self.pathBuilder.checkPointPath / f"{self.mdlId}_checkpoint_loss.h5"),
                                       monitor='val_loss',
                                       verbose=1,
                                       save_best_only=True, #given the spikiness of the obj fct loss hstory, prob better to save only the best
                                       mode='auto')

        return [es1,checkpointLoss]




    def _getLatestFittedMdl(self):
        """ Shitty ass function to determine from which mdl we should continue fitting


        :return:
        """

        listFiles = os.listdir(self.pathBuilder.modelsOutputPath)

        #Has the model already been fit?
        listMatches = [re.match(pattern=f'^model_rf_.*{self.mdlFit.convNetParams.mdlId}.*', string=f) for f in listFiles]
        nonNullMatches = [m.group(0) for m in listMatches if m is not None]

        if len(nonNullMatches) == 0 :

            print("!!Model has never been refit yet!!!\n\n")
            # Check if the model has laready been refit
            self.mdlId = f"rf_{self.numIncrementalEpochs}_{self.mdlFit.convNetParams.mdlId}"  # tweak the mdl name
            mdlToLoad = self.mdlFit.convNetParams.mdlId
        else:

            print("!!Model has been refited - refiting once more!!!\n\n")

            idxLatest = np.argmax(list(map(len, nonNullMatches))) #get the latest model - largest name
            mdlIdOld = nonNullMatches[ idxLatest]
            mdlIdOld=mdlIdOld.split(".h5")[0] #remove the trailing .h5
            mdlIdOld=mdlIdOld.split("model_")[1] #remove the leading model_
            self.mdlId = f"rf_{self.numIncrementalEpochs}_{mdlIdOld}"
            mdlToLoad = mdlIdOld

            if len(self.mdlId) > 150:
                warnings.warn("Warning! the string mdl id is becoming extremely large, watch out!")

        return mdlToLoad



    def _fitMdl(self):

        #Get the latest fitted model id
        mdlIdToLoad= self._getLatestFittedMdl()

        #Read back the existing model
        mdlFile = self.pathBuilder.modelsOutputPath / f"model_{mdlIdToLoad}.h5"
        mdlWeights = self.pathBuilder.modelsOutputPath / f"weights_{mdlIdToLoad}.h5"
        historyPathToLoad = self.pathBuilder.historyOutputPath / f"history_{mdlIdToLoad}.csv"
        historyPathToWrite = self.pathBuilder.historyOutputPath / f"history_{self.mdlId}.csv"

        allFileExits = [os.path.isfile(f) for f in [mdlFile, mdlWeights, historyPathToLoad]]


        if np.all(allFileExits) == False:
            raise Exception("Fatal error trying to retrieve the existing model in mdlFitExisting!")
        else:

            print(f"\n\n!!!!Retraining from existing model!!!!!!\n\n")
            convNetMdlCustom = load_model(str(mdlFile))
            convNetMdlCustom.load_weights(str(mdlWeights))
            mdlHist = pd.read_csv(str(historyPathToLoad))

            # Get the data generators
            dictGenerators = self.dataGeneratorFactory.getTrainTestValidDataGenerators()
            callBackList = self._getCallBacks()

            mdlHist = convNetMdlCustom.fit_generator(generator=dictGenerators['train'],
                                                     validation_data=dictGenerators['valid'],
                                                     epochs=self.numIncrementalEpochs,
                                                     shuffle=self.convNetParams.shuffle,
                                                     callbacks=callBackList,
                                                     verbose=self.convNetParams.verbose)

            # convert the dictionary to a dataframe to standardize behaviour
            mdlHist = pd.DataFrame(mdlHist.history)

            #Write to new history file
            mdlHistOld= pd.read_csv(historyPathToLoad)
            mdlHistConcat=pd.concat([ mdlHistOld.loc[:,['loss',   'val_loss']], mdlHist.loc[:,['loss',   'val_loss']]])
            mdlHistConcat.to_csv(str(historyPathToWrite))

            mdlFileNew = self.pathBuilder.modelsOutputPath / f"model_{self.mdlId}.h5"
            mdlWeightsNew = self.pathBuilder.modelsOutputPath / f"weights_{self.mdlId}.h5"

            # Save the model - save both weights AND model
            convNetMdlCustom.save(str(mdlFileNew))
            convNetMdlCustom.save_weights(str(mdlWeightsNew))


        return convNetMdlCustom
