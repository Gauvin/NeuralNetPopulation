

import unittest
import sys
from keras.applications.vgg19 import VGG19
from keras.optimizers import SGD, Adam
from sklearn.preprocessing import MinMaxScaler


import os
home = os.environ['HOME']
sys.path.append(f"{home}/Projects/NeuralNetPopulation/Python")

from dataGeneration.dataGeneratorFactory import DataGeneratorFactory
from dataGeneration.rawDataInput import RawDataInput

from models.convNetParams import ConvNetParams
from models.mdlFit import MdlFit
from models.mdlFitExisting import MdlFitExisting
from utils.pathBuilder import PathBuilder



rootDir = f'{home}/Projects/NeuralNetPopulation'
provider = 'Mapbox'
gridSize = 100
resp="v_CA16_406"
# final non-linearity: sigmoid to predict [0,1] normalized value vs relu otherwise (since response is non-negative)
#finalNonLin = "sigmoid" if resp == "v_CA16_2540" else "relu"
finalNonLin = 'linear'
optimizerClass = SGD

class TestMdlFit(unittest.TestCase):


    def test_mdl_afit_preFilter(self):
        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=50)  # consider a larger subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        finalNonLin=finalNonLin,
                                        lr=1 * 10 ** -2,
                                        numEpochs=3,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=Adam,
                                        scalerClass=MinMaxScaler,
                                        usePreFilter= True,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit(myDatageneratorFactory,
                          myConvNetParams,
                          myPathBuilder)

        myMdlFit.convNetMdlCustom


    def test_mdl_afit_preFilter_refineFit(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=50)  # consider a larger subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        finalNonLin=finalNonLin,
                                        lr=1 * 10 ** -2,
                                        numEpochs=3,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=Adam,
                                        scalerClass=MinMaxScaler,
                                        usePreFilter= True,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit(myDatageneratorFactory,
                          myConvNetParams,
                          myPathBuilder)

        myMdlFitRefit = MdlFitExisting( myMdlFit,
                                        numIncrementalEpochs = 2  )


    def test_mdl_afit_preFilter_refineFit_v2(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=50)  # consider a larger subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        finalNonLin=finalNonLin,
                                        lr=1 * 10 ** -2,
                                        numEpochs=3,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=Adam,
                                        scalerClass=MinMaxScaler,
                                        usePreFilter= True,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit(myDatageneratorFactory,
                          myConvNetParams,
                          myPathBuilder)

        myMdlFitRefit = MdlFitExisting( myMdlFit,
                                        numIncrementalEpochs = 2  )


    def test_mdl_fit(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=25)  # consider a subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        finalNonLin=finalNonLin,
                                        lr=1 * 10 ** -2,
                                        numEpochs=5,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=optimizerClass,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit( myDatageneratorFactory,
                           myConvNetParams,
                           myPathBuilder )

        myMdlFit.convNetMdlCustom


    def test_mdl_fit_tanh_new_mad(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=25)  # consider a subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        penultimateNonLin ='tanh',
                                        finalNonLin='linear',
                                        lr=1*10**-2,
                                        numEpochs=2,
                                        batchSize=2 ** 5,
                                        lossFct = "mean_absolute_error",
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=optimizerClass,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit( myDatageneratorFactory,
                           myConvNetParams,
                           myPathBuilder )

        myMdlFit.convNetMdlCustom




    def test_mdl_fit_tanhandrelu_new(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=25)  # consider a subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        penultimateNonLin ='tanh',
                                        finalNonLin='relu',
                                        lr=1*10**-2,
                                        numEpochs=2,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=optimizerClass,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit( myDatageneratorFactory,
                           myConvNetParams,
                           myPathBuilder )

        myMdlFit.convNetMdlCustom



    def test_mdl_fit_tanhandrelu_new_adam(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=25)  # consider a subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        penultimateNonLin ='tanh',
                                        finalNonLin='relu',
                                        lr=1*10**-2,
                                        numEpochs=2,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=Adam,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit( myDatageneratorFactory,
                           myConvNetParams,
                           myPathBuilder )

        myMdlFit.convNetMdlCustom


    def test_mdl_fit_tanhandelu_new(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=25)  # consider a subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        penultimateNonLin ='tanh',
                                        finalNonLin='elu', #try to avoid 0 gradient with negative values
                                        lr=1*10**-2,
                                        numEpochs=2,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=optimizerClass,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit( myDatageneratorFactory,
                           myConvNetParams,
                           myPathBuilder )

        myMdlFit.convNetMdlCustom

    def test_mdl_fit_larger(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=50)  # consider a larger subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        finalNonLin=finalNonLin,
                                        lr=1 * 10 ** -2,
                                        numEpochs=10,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=optimizerClass,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit( myDatageneratorFactory,
                           myConvNetParams,
                           myPathBuilder )

        myMdlFit.convNetMdlCustom


    def test_mdl_fit_larger_adam(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=50)  # consider a larger subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        finalNonLin=finalNonLin,
                                        lr=1 * 10 ** -2,
                                        numEpochs=10,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=Adam,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit( myDatageneratorFactory,
                           myConvNetParams,
                           myPathBuilder )

        myMdlFit.convNetMdlCustom

    def test_mdl_fit_larger(self):
        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=50)  # consider a larger subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        finalNonLin=finalNonLin,
                                        lr=1 * 10 ** -2,
                                        numEpochs=10,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=optimizerClass,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit(myDatageneratorFactory,
                          myConvNetParams,
                          myPathBuilder)

        myMdlFit.convNetMdlCustom

