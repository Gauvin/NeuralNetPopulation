


import sys
import pandas as pd
from pytictoc import TicToc
import os
import numpy as np
import geopandas as gpd
from PIL import Image
import pickle
import seaborn as sns
import matplotlib.pyplot as plt
from collections import defaultdict

from dataGeneration.dataGeneratorFactory import DataGeneratorFactory
from utils.logger import Logger

import os
home = os.environ['HOME']
sys.path.append(f"{home}/Projects/NeuralNetPopulation/Python")


class MdlEval(object):

    def __init__(self,
                 mdlFit,
                 convNetParams,
                 pathBuilder,
                 myDatageneratorFactory):

        self.mdlFit = mdlFit
        self.convNetParams = convNetParams
        self.pathBuilder = pathBuilder
        self.myDatageneratorFactory = myDatageneratorFactory

        self.mdlId = self.mdlFit.mdlId

        if isinstance(myDatageneratorFactory,DataGeneratorFactory) == False:
            raise Exception("Fatal error, input a DataGeneratorFactory factry")

        myLogger = Logger(pathBuilder, convNetParams)
        self.logger =  myLogger.logger

        self.dictDf={}
        self.dictRSquare = defaultdict(list)


    def _getMse(self,df):
        return np.mean(df.error ** 2)

    def _getRSquare(self,df):
        sserr = np.sum(df.error ** 2)
        sstotal = np.sum((df.obs - np.mean(df.obs)) ** 2)

        return 1 - (sserr / sstotal)


    def evalModel(self ):

        """ Compute errors and predictions for the train, validation and test sets and add the indices and Geo data


        :return:dictDf : dictionary of pd.DataFrames

        """

        #Determine if the pickle already exists
        picklePath = self.pathBuilder.picklePath / f'dictDf_{self.mdlId}.pickle'
        picklePathRSquare = self.pathBuilder.picklePath / f'dictRSquare_{self.mdlId}.pickle'

        # Get the data generator
        dictTrainTestValid = self.myDatageneratorFactory.getTrainTestValidDataGenerators()

        #Chkec both pickle file existence
        allFilesExists = np.all([os.path.isfile(f) for f in [picklePath, picklePathRSquare]])

        t = TicToc()
        t.tic()


        if allFilesExists :
            print("The pickle file already exists in evalModel")
            with open(str(picklePath), 'rb') as f:
                self.dictDf = pickle.load(f )
            with open(str(picklePathRSquare), 'rb') as f:
                self.dictRSquare = pickle.load(f )
        else:

            for k in dictTrainTestValid.keys():
                # Get the data generator for train, test valid
                dataGenerator = dictTrainTestValid[k]

                # Predictions and mse
                pred = self.mdlFit.convNetMdlCustom.predict(dataGenerator)

                #make sure no excess predictions
                pred=pred[: len(dataGenerator.y)]

                # Input to dataframe
                df = self._createDfPred(pred=pred,
                                        obs=dictTrainTestValid[k].yScaled,  #don't forget to take the scaled/transformed response
                                        id=k)

                #Now merge on the initial dfGrid to add just about any variable
                df['index']=dataGenerator.indices[: len(dataGenerator.y)]
                dfGrid = dataGenerator.dfGrid.reset_index()
                df = pd.merge(df, dfGrid, on='index')

                self.dictDf[k] = df

                # Create a dict for the rsquare et al.
                self.dictRSquare[k].append(self._getMse(df))
                self.dictRSquare[k].append(np.std(df.obs) ** 2)
                self.dictRSquare[k].append(self._getRSquare(df))




            # Pickle the results
            with open(str(picklePath), 'wb') as f:
                pickle.dump(self.dictDf,f,protocol=pickle.HIGHEST_PROTOCOL)

            with open(str(picklePathRSquare), 'wb') as f:
                pickle.dump(self.dictRSquare, f, protocol=pickle.HIGHEST_PROTOCOL)


        #Write to file
        pathErrorCsv = self.pathBuilder.predictionErrorPath / f'{self.mdlId}_all.csv'
        dfAll = pd.concat(self.dictDf).reset_index()
        dfAll.to_csv(str(pathErrorCsv))

        # Convert to df + write to csv
        dfRSquare = pd.DataFrame.from_dict(self.dictRSquare )
        dfRSquare['id'] = ['mse', 'variance', 'rsquare']
        dfRSquareT = dfRSquare.set_index('id').T
        dfRSquareT.to_csv(self.pathBuilder.predictionErrorPath / f'{self.mdlId}_rsquareetal.csv')

        totalTime = t.toc()

        self.logger.warning( f"\n\nTotal time taken to eval the model {totalTime}\n\n")


        return self.dictDf

    def _plotAllRepresentativeImages(self,colsToSelect=['error','pred']):

        """ Create a mosaic with the images with the min/max values associated with colsToSelect


        :param colsToSelect: list of str
        :return:
        """

        colStr = ",".join(colsToSelect)
        listDfCsv=[]
        f=plt.figure(figsize=(20,10))
        plotCounter=1
        for ex in ['min','max']:
            for c in colsToSelect:
                for id in ['train','test']:

                    try:
                        im, df= self._plotRepresentativeImages(ex,c,id)
                        listDfCsv.append(df)

                        plt.subplot(len(['train','test']), len(colsToSelect)*len(['min','max']),plotCounter)
                        plt.imshow(im)

                        err=round(df.error.values[0],2)
                        pred=round(df.pred.values[0],2)
                        obs =round(df.obs.values[0],2)
                        neigh=df.Q_socio.values[0]
                        plt.title(f'{ex} {c} - {id} ({neigh})\npred: {pred}\n obs {obs}\n err: {err} ')
                    except Exception as e:
                        print(f"Error with {ex} {c} - {id} -- skipping it")

                    plotCounter= plotCounter+1

        plt.tight_layout()

        #Save the image
        f.savefig(  self.pathBuilder.figuresOutputPath / "RepresentativeImages" / f"representativeImg_{colStr}_{self.mdlId}.png" )


        #Also  save the associated csv data for later inspection
        dfAll = pd.concat(listDfCsv)
        dfAll.to_csv( self.pathBuilder.figuresOutputPath / "RepresentativeImages" / f"representativeImg__{colStr}_{self.mdlId}.csv" )



    def _plotRepresentativeImages(self,extrema, colToSelect, id):
        """Get the images that seem the most representative to the model:highest and lowest predictons

        """
        dfAll = pd.concat(self.dictDf).reset_index()

        if np.isin(extrema, ['min','max']).any() == False:
            raise Exception("Fatal error when plotting a representative image use min or max")
        if np.isin(colToSelect, dfAll.columns).any() == False:
            raise Exception("Fatal error when plotting a representative image use a valid column")
        if np.isin(id,list(self.dictDf.keys())).any() == False:
            raise Exception("Fatal error when plotting a representative image use train,valid or test datasets")

        idxId = dfAll['id'] == id

        if np.sum(idxId) == 0:
            raise Exception ("Fatal error in _plotRepresentativeImages - No train test to select!")

        #Get the min error in abs value, but the min pred (since we can predict negative values...)
        dfID = dfAll.loc[idxId, :]
        if colToSelect =='error':
            idxExtrema = dfID[colToSelect].abs().min() == dfID[colToSelect].abs() if extrema == 'min' else  dfID[colToSelect].abs().max() == dfID[colToSelect].abs()
        else:
            idxExtrema = dfID[colToSelect].min() == dfID[colToSelect] if extrema == 'min' else dfID[colToSelect].max() == dfID[ colToSelect]

        if np.sum(idxExtrema) == 0:
            raise Exception ("Fatal error in _plotRepresentativeImages - No extrema to select!")

        #Read the image
        sourcePath = dfID.loc[ idxExtrema  , "pathImg"].values[0]
        im=Image.open(sourcePath)

        #Also return the associated csv data for later inspection
        df = dfID.loc[idxExtrema,:]

        return im, df

    def plotErrorPredDistribution(self):

        """ Main wrapper for all error/prediction plots


        :return:
        """

        #Make sure the results have been computed
        if self.dictDf == {}:
            self.evalModel()

        self._plotAllRepresentativeImages()

        self._plotErrorSpatial(plotCentroid=True)
        self._plotErrorSpatial(plotCentroid=False)

        self._plotPredVsErrorObs(plotResp=True)
        self._plotPredVsErrorObs(plotResp=False)

        self._plotErrorPredDist()

        self._plotBarByNeigh()



    def _plotErrorPredDist(self):
        """ Plot the distribution with a grid + distplot -- rows: (error,pred) X col: id (train,test,valid)

        :return:
        """

        plt.figure(figsize=(20, 15))
        plt.title(f"Prediction errors\n{self.convNetParams.numEpochs} epochs",
                  fontsize=20)

        # Concat the train,test,valid df + Melt the df by pivoting on the id (train,test,valid)
        dfRaw = pd.concat(self.dictDf)
        dfRaw = dfRaw.loc[:, ['pred', 'obs', 'error', 'id','Q_socio']]
        dfMelted = pd.melt(dfRaw, id_vars=['id','Q_socio'])

        # Facet wrap and plot wth rows given by obs, pred or error and col is train or test
        g = sns.FacetGrid(dfMelted, col="id", row='variable', hue="id", size=4, sharey=False,
                                   sharex=False)  # free xand y scale
        g.map(sns.distplot, 'value')

        g.savefig(self.pathBuilder.figuresOutputPath / "PredictionsErrors" / f"error_pred_train_test_{self.mdlId}.png")



    def _plotErrorSpatial(self, plotCentroid):

        """ Plot the spatial error distribution (by DA)


        :return:
        """

        #Plot some info on what will be plot
        strPlot = "Plotting the DA centroids" if plotCentroid else "Plotting the entire DA polygons"
        print(strPlot)

        # Would be better to use some sort of spatial interpolation, rather than averaging over all grids touching the DA
        dfRaw= pd.concat(self.dictDf)
        dfByDA = dfRaw.groupby("GeoUID").agg({"error": lambda x: np.nan if x.isnull().all() else np.mean(x.dropna()),
                                              'pred' : lambda x: np.nan if x.isnull().all() else np.mean(x.dropna()),
                                              "Q_socio": lambda x: ",".join(np.unique(x)),
                                              'id': lambda x:  np.unique(x)[0] }).reset_index()  # wtf pandas, do I really need to implement this shit?
        dfByDA['GeoUID'] = dfByDA['GeoUID'].astype(str).astype('int64')  # ed to convert, otherwise GeoUID is treated as an int

        dfRaw=dfByDA.loc[ :, ['GeoUID', 'Q_socio', 'error', 'pred', 'id']]
        dfMelted = pd.melt(dfRaw, id_vars=['GeoUID', 'Q_socio', 'id'])

        shpNeigh = self.myDatageneratorFactory.rawDataInput.shpNeigh
        shpCensus=self.myDatageneratorFactory.rawDataInput.shpCensus
        shpCensusMerged = pd.merge( shpCensus,dfMelted, how="left" )
        shpCensusMerged.dropna(inplace=True) #not all DAs are used in test or train

        # Cannot facetwrap, need to loop over train, test and error, pred
        f, axes = plt.subplots( figsize=(15, 10),
                                nrows=len(shpCensusMerged.variable.unique()),
                                ncols=len(shpCensusMerged.id.unique())
                                )

        #Don't add the neigh boundaries: masks the errors
        shpNeighDisolved=shpNeigh
        shpNeighDisolved['city'] = 'mtl'
        shpNeighDisolved=shpNeighDisolved.dissolve(by='city')

        for j, id in enumerate(shpCensusMerged.id.unique() ) : #iterate over train, test, valid
            for i, var in enumerate(shpCensusMerged.variable.unique() ) : #iterate over error or pred

                # Plot the spatial distribution of the errors
                idx= (shpCensusMerged.variable == var) & (shpCensusMerged.id == id)

                shpNeighDisolved.plot(
                              linewidth=0.5,
                              facecolor='white',
                              legend=False,
                              linestyle='dotted',
                              edgecolor='black',
                              ax= axes[i][j] ,
                            zorder=1
                )

                #Subset to keep the train,test + error or pred
                shpCensusMergedFiltered=shpCensusMerged.loc[idx, : ]

                #Select centroids or the original polygons
                if plotCentroid:
                    shpCentroidFiltered = gpd.GeoDataFrame( shpCensusMergedFiltered, geometry=shpCensusMergedFiltered.centroid)
                    shpToPlot = shpCentroidFiltered
                else:
                    shpToPlot = shpCensusMergedFiltered


                shpToPlot.plot(column=shpToPlot.value,
                                                 cmap='coolwarm',
                                                 legend=True,
                                                 ax= axes[i][j],
                                                 vmin=-1, #fix the upper and lower bounds so the values are coherent accross all plots
                                                 vmax=1,
                                                 zorder=2)

                axes[i][j].set_title(f"{var}\n{id}")

        f.suptitle(f"Predictions and errors by DA", fontsize=16)
        f.savefig(self.pathBuilder.figuresOutputPath /  "ErrorsSpatial" / f"spatialErrors_centroid_{plotCentroid}_{self.mdlId}.png")



    def _plotBarByNeigh(self):

        """Bar plot of errors by neigh

        """


        # Concat the train,test,valid df + Melt the df by pivoting on the id (train,test,valid)
        dfRaw = pd.concat(self.dictDf)
        dfRaw = dfRaw.loc[:, ['pred', 'obs', 'error', 'id', 'Q_socio']]

        dfByNeigh = dfRaw.loc[ dfRaw.id=='train'] .groupby(["Q_socio"]).agg({"error": np.median}).sort_values("error").reset_index()
        dfByNeigh.rename(columns={ "error" :  "median error"}, inplace=True)
        dfByNeigh.sort_values(by="median error")

        dfRaw['Q_socio']= dfRaw['Q_socio'].astype('category') #fuck you go fuck yourself pandas
        dfRaw['Q_socio'] = pd.Categorical(dfRaw['Q_socio'],
                                          categories=dfByNeigh['Q_socio'],
                                          ordered=True)

        g = sns.catplot(x="Q_socio", y="error",   hue='id',  kind="box", data=dfRaw )
        g.fig.set_figwidth(10)
        g.fig.set_figheight(7)
        #g.map(sns.swarmplot, 'Q_socio', 'error', color='0.25')

        g.set_xticklabels(rotation=90)

        g.fig.suptitle("Error by neighbourhood")
        plt.tight_layout()

        g.savefig(self.pathBuilder.figuresOutputPath / "ErrorsBarByNeigh" / f"barByNeighErrors_{self.mdlId}.png")


    def _plotPredVsErrorObs(self, plotResp):
        """Plot the pred vs obs


        :return:
        """

        dfRaw = pd.concat(self.dictDf)
        dfRaw = dfRaw.loc[:, ['GeoUID', 'Q_socio', 'error', 'pred', 'id', self.convNetParams.resp]]

        dfRaw.sort_values(by=["id", "error", self.convNetParams.resp], inplace=True)
        dfRaw.reset_index(inplace=True)

        yVal = self.convNetParams.resp if plotResp else "error"
        benchmark = dfRaw[yVal].mean() if plotResp else 0
        subdirName =  "PredictionsVsObs" if plotResp else "PredictionsVsErr"
        strName = "resp" if plotResp else "error"


        g = sns.FacetGrid(hue='Q_socio', row="id", data=dfRaw)

        g.map(sns.scatterplot, 'pred', yVal)

        # add a marker for the mean observed value for that data set
        # benchmark
        g.map(plt.axhline, y=benchmark, ls=":", c=".5")
        g.add_legend()

        g.fig.set_figwidth(10)
        g.fig.set_figheight(7)

        #don't use the tight_layout
        g.fig.suptitle( f"Prediction vs {yVal} \n{self.convNetParams.numEpochs} epochs\n\n", fontsize=14 ,x=0.6)
        g.savefig(self.pathBuilder.figuresOutputPath / subdirName / f"predVs{strName}_{self.mdlId}.png")



    def _createDfPred(self, pred, obs, id):

        pathErrorCsv = self.pathBuilder.predictionErrorPath / f'{self.mdlId}_{id}.csv'

        if os.path.isfile(pathErrorCsv):
            df = pd.read_csv(pathErrorCsv)
            print(f'File {pathErrorCsv} already exists => loading it')
        else:
            print(f'File {pathErrorCsv} does not exist')

            # Quick check -> predictions and observations must be a column vector
            map(self._checkDim, [pred, obs])

            # Create the dataframe
            if len( pred.ravel()) != len(obs.ravel()):
                print("Warning! predict is not returning the correct number of observations => something to do witht the len of the data generator being to small => truncating the last observations")
            df = pd.DataFrame.from_dict({'pred': pred.ravel(), 'obs': obs[: len(pred)].ravel()},
                                        orient='columns')

            #Get the prediction error
            df['error'] = df['obs'] - df['pred']

            #Get the identification
            df['id'] = id

            #Write the csv
            df.to_csv(str(pathErrorCsv))

        return df



    def _checkDim(self, arr):

        if len(arr.shape) == 1:
            return True
        elif len(arr.shape) == 2 and arr.shape[1] == 1:
            return True
        else:
            raise Exception("The array should be a row or column vector")

        return False
