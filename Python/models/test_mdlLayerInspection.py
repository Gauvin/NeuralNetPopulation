

import unittest
import sys
from keras.applications.vgg19 import VGG19
from keras.optimizers import SGD, Adam
from sklearn.preprocessing import MinMaxScaler

import os
home = os.environ['HOME']
sys.path.append(f"{home}/Projects/NeuralNetPopulation/Python")

from dataGeneration.dataGeneratorFactory import DataGeneratorFactory
from dataGeneration.rawDataInput import RawDataInput

from models.convNetParams import ConvNetParams
from models.mdlFit import MdlFit
from models.mdlLayerInspection import MdlLayerInspection
from models.mdlFitExisting import MdlFitExisting

from utils.pathBuilder import PathBuilder



rootDir = f'{home}/Projects/NeuralNetPopulation'
provider = 'Mapbox'
gridSize = 100
resp="v_CA16_406"
finalNonLin = 'linear'
optimizerClass = SGD

class TestMdlLyerInspection(unittest.TestCase):


    def test_mdl_inspection_tanh(self):
        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=25)  # consider a subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        penultimateNonLin='tanh',
                                        finalNonLin='linear',
                                        lr=1 * 10 ** -2,
                                        numEpochs=5,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=optimizerClass,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit(myDatageneratorFactory,
                          myConvNetParams,
                          myPathBuilder)

        myMdlLayerInspection = MdlLayerInspection(myDatageneratorFactory,
                                                  myConvNetParams,
                                                  myPathBuilder,
                                                  myMdlFit)

        myMdlLayerInspection.plotTsneRepLastLayerAll()


    def test_mdl_inspection_tanh_refit(self):
        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=25)  # consider a subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        penultimateNonLin='tanh',
                                        finalNonLin='linear',
                                        lr=1 * 10 ** -2,
                                        numEpochs=5,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=optimizerClass,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit(myDatageneratorFactory,
                          myConvNetParams,
                          myPathBuilder)

        myMdlFitRefit = MdlFitExisting( myMdlFit,
                                        numIncrementalEpochs = 2  )
        myMdlFit = myMdlFitRefit

        myMdlLayerInspection = MdlLayerInspection(myDatageneratorFactory,
                                                  myConvNetParams,
                                                  myPathBuilder,
                                                  myMdlFit)

        myMdlLayerInspection.plotTsneRepLastLayerAll()


    def test_mdl_inspection_tanh_batch_size(self):
        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=76)  # consider a subset of the data, but make it just a bit larger than the batch size to see if error - watch out for train,test,valid partition

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        penultimateNonLin='tanh',
                                        finalNonLin='linear',
                                        lr=1 * 10 ** -2,
                                        numEpochs=5,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=optimizerClass,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit(myDatageneratorFactory,
                          myConvNetParams,
                          myPathBuilder)




        myMdlLayerInspection = MdlLayerInspection( myDatageneratorFactory,
                                                   myConvNetParams,
                                                   myPathBuilder,
                                                   myMdlFit)

        myMdlLayerInspection.plotTsneRepLastLayerAll()

        myMdlLayerInspection.plotDistributionAllFCDim()

