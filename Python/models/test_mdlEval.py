
import unittest
import sys
from keras.applications.vgg19 import VGG19
from keras.optimizers import SGD, Adam
from keras.models import load_model
from sklearn.preprocessing import MinMaxScaler
import numpy as np
import os

import os
home = os.environ['HOME']
sys.path.append(f"{home}/Projects/NeuralNetPopulation/Python")

from dataGeneration.dataGeneratorFactory import DataGeneratorFactory
from dataGeneration.rawDataInput import RawDataInput

from models.convNetParams import ConvNetParams
from models.mdlFit import MdlFit
from models.mdlFitExisting import MdlFitExisting
from models.mdlEval import MdlEval

from utils.pathBuilder import PathBuilder



rootDir = f'{home}/Projects/NeuralNetPopulation'
provider = 'Mapbox'
gridSize = 100
resp="v_CA16_406"
# final non-linearity: sigmoid to predict [0,1] normalized value vs relu otherwise (since response is non-negative)
#finalNonLin = "sigmoid" if resp == "v_CA16_2540" else "relu"
finalNonLin = 'linear'
optimizerClass = SGD




class TestMdlEval(unittest.TestCase):

    def test_mdl_eval_small(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=25)  # consider a subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        finalNonLin=finalNonLin,
                                        lr=1*10**-3,
                                        numEpochs=2,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=optimizerClass,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit( myDatageneratorFactory,
                           myConvNetParams,
                           myPathBuilder )



        # Get a small subset of data in memory to speed things up
        myMdlEval =  MdlEval( myMdlFit,
                              myConvNetParams,
                              myPathBuilder,
                              myDatageneratorFactory)

        #Get the prediction
        dictDf = myMdlEval.evalModel( )

        #Plot the distribution
        myMdlEval.plotErrorPredDistribution()



    def test_mdl_eval_small_again_for_pickle(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=25)  # consider a subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        finalNonLin=finalNonLin,
                                        lr=1*10**-3,
                                        numEpochs=2,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=optimizerClass,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit( myDatageneratorFactory,
                           myConvNetParams,
                           myPathBuilder )



        # Get a small subset of data in memory to speed things up
        myMdlEval =  MdlEval( myMdlFit,
                              myConvNetParams,
                              myPathBuilder,
                              myDatageneratorFactory)

        #Get the prediction
        dictDf = myMdlEval.evalModel( )

        #Plot the distribution
        myMdlEval.plotErrorPredDistribution()



    def test_mdl_eval_small_refit(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=25)  # consider a subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        finalNonLin=finalNonLin,
                                        lr=1*10**-3,
                                        numEpochs=2,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=optimizerClass,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit( myDatageneratorFactory,
                           myConvNetParams,
                           myPathBuilder )

        #Refit
        myMdlFitRefit = MdlFitExisting(myMdlFit,
                                       numIncrementalEpochs=2)
        myMdlFit = myMdlFitRefit

        # Get a small subset of data in memory to speed things up
        myMdlEval =  MdlEval( myMdlFit,
                              myConvNetParams,
                              myPathBuilder,
                              myDatageneratorFactory)

        #Get the prediction
        dictDf = myMdlEval.evalModel( )

        #Plot the distribution
        myMdlEval.plotErrorPredDistribution()


    def test_mdl_eval_small_refit_power_2(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=25)  # consider a subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        finalNonLin=finalNonLin,
                                        lr=1*10**-3,
                                        numEpochs=2,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=optimizerClass,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit( myDatageneratorFactory,
                           myConvNetParams,
                           myPathBuilder )

        #Refitmodel that was already refitted
        myMdlFitRefit = MdlFitExisting(myMdlFit,
                                       numIncrementalEpochs=3)
        myMdlFit = myMdlFitRefit

        # Get a small subset of data in memory to speed things up
        myMdlEval =  MdlEval( myMdlFit,
                              myConvNetParams,
                              myPathBuilder,
                              myDatageneratorFactory)

        #Get the prediction
        dictDf = myMdlEval.evalModel( )

        #Plot the distribution
        myMdlEval.plotErrorPredDistribution()


    def test_mdl_eval_tanh(self):
        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=25)  # consider a subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        penultimateNonLin='tanh',
                                        finalNonLin='linear',
                                        lr=1 * 10 ** -2,
                                        numEpochs=2,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=optimizerClass,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit(myDatageneratorFactory,
                          myConvNetParams,
                          myPathBuilder)

        # Get a small subset of data in memory to speed things up
        myMdlEval =  MdlEval( myMdlFit,
                              myConvNetParams,
                              myPathBuilder,
                              myDatageneratorFactory)


        #Plot the distribution -- should call predict by default if the predictions have not already been computed
        myMdlEval.plotErrorPredDistribution()


    def test_mdl_eval_tanh_batch_size(self):
        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=76)  # consider a subset of the data, but make it just a bit larger than the batch size to see if error - watch out for train,test,valid partition

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        penultimateNonLin='tanh',
                                        finalNonLin='linear',
                                        lr=1 * 10 ** -2,
                                        numEpochs=5,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0,
                                        convNetClass=VGG19,
                                        optimizerClass=optimizerClass,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        myMdlFit = MdlFit(myDatageneratorFactory,
                          myConvNetParams,
                          myPathBuilder)

        # Get a small subset of data in memory to speed things up
        myMdlEval =  MdlEval( myMdlFit,
                              myConvNetParams,
                              myPathBuilder,
                              myDatageneratorFactory)


        #Plot the distribution -- should call predict by default if the predictions have not already been computed
        myMdlEval.plotErrorPredDistribution()