


from keras.layers import Input, Dense,  Flatten, Dropout, MaxPooling2D, Conv2D
from keras.models import Model
from keras.models import load_model
from keras.regularizers import l1
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.optimizers.schedules import ExponentialDecay
from keras.initializers import Constant
import keras.backend as K


import matplotlib.pyplot as plt
import seaborn as sns


import warnings
import sys
import os
import pandas as pd
import numpy as np
import math


import os
home = os.environ['HOME']
sys.path.append(f"{home}/Projects/NeuralNetPopulation/Python")



class MdlFit(object):
    
    def __init__(self,
                dataGeneratorFactory,
                convNetParams,
                pathBuilder ):
        
 
        self.dataGeneratorFactory = dataGeneratorFactory
        self.convNetParams=convNetParams
        self.pathBuilder = pathBuilder

        self.mdlId= convNetParams.mdlId

        #For the prefilter part
        #Hard code this shit - could only change a few params anyways - (not the number of channels)
        self.filterSize = 3
        self.poolFilterSize = 3
        self.numFilters = 3
        self.stridePooling = 2

        self.convNetMdlCustom = self._buildModelPreFilter() if convNetParams.usePreFilter else  self._buildModel()
        self.convNetMdlCustom = self._fitMdl()

        try:
            self._plotHistory()
        except Exception as e:
            print(f"Error printing in MdlFit - {e}")

        self.numParams = None




    def _getInputVGDim(self):

        d = math.ceil((self.convNetParams.dim[0] - self.poolFilterSize) / self.stridePooling)

        newDim = (d, d,self.numFilters)

        return newDim



    def _plotHistory(self):
        print("Plotting the history")

        historyPath = self.pathBuilder.historyOutputPath / f"history_{self.mdlId}.csv"
        figureHistoryPath = self.pathBuilder.figuresOutputPath / 'LossFctHistory' / f'mdlHistory_{self.convNetParams.mdlId}.png'

        dfHist = pd.read_csv(str(historyPath))

        #Data manips
        dfHist.rename( columns={dfHist.columns[0] : "epoch"}, inplace=True)
        dfHist['epoch'] = np.arange(dfHist.shape[0])
        dfMelted=dfHist.loc[:,['epoch',  'loss',  'val_loss' ]].melt(id_vars='epoch')


        #Get the quantiles and truncate to avoid excessively large values + compute log for better visualization
        quant = np.quantile(dfMelted['value'], q=0.95)
        idxToTrunc = dfMelted['value'] > quant
        dfMelted.loc[idxToTrunc, 'value'] = quant
        dfMelted['logloss'] = dfMelted['value'].apply(lambda x: np.log(x))

        #Now plot it
        f = plt.figure(figsize=(7, 7))
        sns.lineplot(x='epoch', y='logloss', hue='variable', data=dfMelted )
        plt.title(
            f"Objection function history - {self.convNetParams.resp} \nLoss (5% largest outliers truncated - {self.convNetParams.numImgs} images)\n{self.convNetParams.numEpochs} iterations")
        f.savefig(figureHistoryPath)



    def _buildModelPreFilter(self):

        print("Considering a model WITH prefilter")

        #Instantiate the conv net (vgg, resnet, etc.)
        with warnings.catch_warnings():
            convNetExtractorSmaller = self.convNetParams.convNetClass(include_top=False,
                                            weights='imagenet',
                                            classes=1,
                                            input_shape=self._getInputVGDim())

            convNetExtractorInit = self.convNetParams.convNetClass(include_top=False,
                                         weights='imagenet',
                                         classes=1,
                                         input_shape=self.convNetParams.dim)



        #Add in a small pre filter at the beginning to reduce the image size
        inputTensorFeatExtractor = convNetExtractorInit.layers[0].input

        #Replicate the exact same blocks as for the rest of the vgg - 3 3x3xnumFilters + relu stacked with pooling at the end
        convOutput1 = Conv2D(filters=self.numFilters, kernel_size=self.filterSize, activation='relu',  padding='same')(inputTensorFeatExtractor)
        convOutput2 = Conv2D(filters=self.numFilters, kernel_size=self.filterSize, activation='relu',  padding='same')(convOutput1)
        convOutput3 = Conv2D(filters=self.numFilters, kernel_size=self.filterSize, activation='relu',  padding='same')(convOutput2)
        poolOutput = MaxPooling2D(pool_size=self.poolFilterSize, strides=self.stridePooling)(convOutput3)

        #Add back the VGG part - without the input + freeze the preexistinf architecture
        inp = poolOutput
        for l in convNetExtractorSmaller.layers[1:]:
            l.trainable = False
            out = l(inp)
            inp = out

        #Build the model and print some info
        # Flatten them + use these for prediction
        flatLyr = Flatten(name='flatten')(inp)

        penultimateDenseLyr = Dense(units=16,  # usually 2**4
                                        activation='relu',
                                        kernel_initializer='he_uniform',
                                        name='fc1')(flatLyr)

        # Just one fully connected layers
        # Try with L1 norm one and/or drop out if evidence of overfiting an poor generalsation power
        finalDenseLyr = Dense(units=16,  # usually 2**4
                                  activation='relu',
                                  kernel_initializer='he_uniform',
                                  name='fc2',
                                  )(penultimateDenseLyr)

        finalOutput = Dense(1,
                                name='predictions',
                                activation='linear',
                                kernel_initializer='he_uniform')(finalDenseLyr)

        newModel=Model(inputTensorFeatExtractor, finalOutput)

        if self.convNetParams.verbose:
                print("Pre filter model")
                newModel.summary()   # (3*27 + 3) * 3 =272 trainable parameters - 3 layers of [3 (number filters) 3X3X3 conv + 3]

        return  newModel



    def _buildModel(self):
        '''Using the base feature extractor, 1-flatten 2- add a fc layer 3 (optional) add some regularization 4-set the final activation (depending on the response)
        
        Returns:
            convNetMdlCustom: tensorflow.python.keras.engine.training.Model
        
        '''
        #Instantiate the conv net (vgg, resnet, etc.)

        print("Considering a model without prefilter")

        with warnings.catch_warnings () :
            convNetExtractor = self.convNetParams.convNetClass(include_top=False,
                                           weights='imagenet',
                                           input_shape= self.convNetParams.dim)
        if self.convNetParams.verbose:
            convNetExtractor.summary()
            
        
        #Fix the base layers-> use for feature extraction
        numLayers=len(convNetExtractor.layers)
        for k, layer in enumerate(convNetExtractor.layers):
            layer.trainable = False

        # Conv net/feature extractor output- check out the artRecognition.mdlFit.mdlFinal.py for an alternative less verbose tehnique
        inputTensorFeatExtractor = convNetExtractor.layers[0].input
        outputTensorFeatExtractor = convNetExtractor.layers[len(convNetExtractor.layers)-1].output
        mdlFeatExtractor = Model(inputTensorFeatExtractor, outputTensorFeatExtractor)
        outputMdlInit=mdlFeatExtractor(inputTensorFeatExtractor)

        #Flatten them + use these for prediction
        flatLyr = Flatten(name='flatten')(outputMdlInit)

        penultimateDenseLyr = Dense(units=self.convNetParams.penultimateFcUnits , #usually 2**4
                              activation=self.convNetParams.secondLastNonLin,
                              kernel_initializer='he_uniform',
                              name='fc1' ,
                              activity_regularizer=l1(self.convNetParams.l1Coeff))(flatLyr)

        #Just one fully connected layers 
        #Try with L1 norm one and/or drop out if evidence of overfiting an poor generalsation power 
        finalDenseLyr = Dense(units=self.convNetParams.finalFcUnits , #usually 2**4
                              activation=self.convNetParams.penultimateNonLin,
                              kernel_initializer='he_uniform',
                              name='fc2' , 
                              activity_regularizer=l1(self.convNetParams.l1Coeff))(penultimateDenseLyr)
        


        #Only 1 class
        if self.convNetParams.lossFct == 'mean_absolute_error':
            print(f"setting bias activation for final layer to median -- benchmark : {self.dataGeneratorFactory.benchmarkConstantPredMed}")
            offset = Constant(self.dataGeneratorFactory.yTrainMed)#0.10981786568779056
        elif self.convNetParams.lossFct == 'mean_squared_error' :
            print(f"setting bias activation for final layer to mean -- benchmark : {self.dataGeneratorFactory.benchmarkConstantPredMean}")
            offset =  Constant(self.dataGeneratorFactory.yTrainMean)  #0.15830979035625173
        else:
            print("default bias activation for final layer")
            offset = Constant(0)


        finalOutput = Dense(1,
                            name='predictions',
                            activation=self.convNetParams.finalNonLin,
                            kernel_initializer='he_uniform',
                            bias_initializer=offset)(finalDenseLyr)

        #Final model
        convNetMdlCustom = Model(inputTensorFeatExtractor, finalOutput )


        #Set the loss
        optInst=self._getOptimizer()
        convNetMdlCustom.compile(loss=self.convNetParams.lossFct , optimizer=optInst)

        #Get the number of params
        self.numParams=sum([np.prod(K.get_value(w).shape) for w in convNetMdlCustom.trainable_weights] )
        
        #Print summary and details if required
        if self.convNetParams.verbose:
            convNetMdlCustom.summary()
            print(f"Total  number of trainable params: {self.numParams}")

        return convNetMdlCustom


    def _getOptimizer(self):

        lr_schedule = ExponentialDecay(
            self.convNetParams.lr, #needs a relatively high value, otherwise will surely stagnate very fast
            decay_steps=10**5, #decay each number of steps
            decay_rate=0.96, #quickly decaying
            staircase=True) # strictly monotonic decreasing if False

        optInst = self.convNetParams.optimizerClass(learning_rate=lr_schedule)


        return optInst


    def _getCallBacks(self):
        # Early stopping for the evaluation loss
        es1 = EarlyStopping(monitor='val_loss',
                            verbose=1,
                            min_delta=10 ** -4,
                            patience=int(self.convNetParams.numEpochs*0.75)) #Very patient: allow for 75% of full iterations

        checkpointLoss = ModelCheckpoint(str( self.pathBuilder.checkPointPath / f"{self.mdlId}_checkpoint_loss.h5"),
                                       monitor='val_loss',
                                       verbose=1,
                                       save_best_only=True, #given the spikiness of the obj fct loss hstory, prob better to save only the best
                                       mode='auto')

        return [es1,checkpointLoss]



    def _fitMdl(self):
        
        
        mdlFile = self.pathBuilder.modelsOutputPath / f"model_{self.mdlId}.h5"
        mdlWeights = self.pathBuilder.modelsOutputPath / f"weights_{self.mdlId}.h5"
        historyPath = self.pathBuilder.historyOutputPath / f"history_{self.mdlId}.csv"

        allFileExits = [os.path.isfile(f) for f in [mdlFile,mdlWeights,historyPath ] ]

        if np.all(allFileExits) :

            print(f"\n\n!!!!Loading existing model {self.mdlId}!!!!!!\n\n")
            convNetMdlCustom = load_model(str(mdlFile))
            mdlHist = pd.read_csv(str(historyPath))

        else:
            
            #Get the data generators
            dictGenerators= self.dataGeneratorFactory.getTrainTestValidDataGenerators()
            convNetMdlCustom = self._buildModel()
            callBackList = self._getCallBacks()

            mdlHist = convNetMdlCustom.fit_generator(generator=dictGenerators['train'],
                                               validation_data=dictGenerators['valid'],
                                               epochs=self.convNetParams.numEpochs,
                                               shuffle=self.convNetParams.shuffle,
                                               callbacks=callBackList,
                                               verbose=self.convNetParams.verbose)

            #convert the dictionary to a dataframe to standardize behaviour
            mdlHist= pd.DataFrame(mdlHist.history)
            mdlHist.to_csv(str(historyPath))


            # Save the model - save both weights AND model
            convNetMdlCustom.save(str(mdlFile))
            convNetMdlCustom.save_weights(str( mdlWeights))


        return convNetMdlCustom
