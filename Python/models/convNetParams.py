

from keras.applications.vgg19 import VGG19
from sklearn.preprocessing import MinMaxScaler
from keras.optimizers import SGD
from pathlib import Path
import os

class ConvNetParams(object):
    
    
    def __init__(self,
                 provider,
                 gridSize,
                 resp,
                 dim,
                 numImgs,
                 finalNonLin,
                 penultimateNonLin='relu',
                 penultimateFcUnits = None,
                 secondLastNonLin=None,
                 lr = 5*10**-3,
                 numEpochs=20 ,
                 batchSize=2**5 ,
                 l1Coeff = 0 ,
                 dropoutRate=0.1,
                 finalFcUnits=2**4,
                 convNetClass = VGG19,
                 optimizerClass = SGD,
                 scalerClass =MinMaxScaler,
                 lossFct='mean_squared_error',
                 usePreFilter = False,
                 propTrain=0.8,
                 propVal=0.15,
                 extraStrId="",
                 shuffle=False,
                 verbose=True):

            self.provider = provider
            self.gridSize = gridSize
            self.resp = resp
            self.convNetName = convNetClass.__name__
            self.dim=dim
            self.numImgs=numImgs
            self.finalNonLin = finalNonLin
            self.penultimateNonLin = penultimateNonLin
            
            self.lr =lr
            self.numEpochs=numEpochs 
            self.batchSize=batchSize
            self.l1Coeff = l1Coeff
            self.dropoutRate=dropoutRate
            self.finalFcUnits=finalFcUnits
            self.convNetClass = convNetClass
            self.optimizerClass = optimizerClass
            self.optimizerName = optimizerClass.__name__
            self.scalerClass=scalerClass
            self.scalerName = scalerClass.__name__
            self.lossFct=lossFct
            self.usePreFilter = usePreFilter
            self.propTrain = propTrain
            self.propVal = propVal
            self.extraStrId=extraStrId
            self.shuffle = shuffle
            self.verbose=verbose

            self.secondLastNonLin = secondLastNonLin if secondLastNonLin is not None else self.penultimateNonLin
            self.penultimateFcUnits = penultimateFcUnits if penultimateFcUnits is not None else self.finalFcUnits

            self.mdlId = self._getMdlName()
            
            
            
    def _getMdlName(self):
        
        mdlId = f"{self.resp}_{self.convNetName}_" \
                f"grS_{self.gridSize}" \
                f"nEpo_{self.numEpochs}" \
                f"nImg_{self.numImgs}_opt_{self.optimizerName}" \
                f"flNoLi_{self.finalNonLin}_pUNoLi_{self.penultimateNonLin}" \
                f"lFct_{self.lossFct}_flFcUn_{self.finalFcUnits}_" \
                f"sdLstNonLi_{self.secondLastNonLin}_pUFcUn_{self.penultimateFcUnits}" \
                f"pFil_{self.usePreFilter}"

 
        if os.path.exists( Path("Projects", "NeuralNetPopulation","Data","Models","FinalModel") / f"model_{mdlId}.h5"):
                print("Model already exists!")
        else:
                print("Fitting a new model - model does not exits")

        if len(mdlId) > 150:
                print(f"Warning! mdl name shouldnot exceed 150 characters! it is {len(mdlId)} presently")
                #raise Exception(f"Fatal error with mdl name! cannot exceed 120 characters! it is {len(mdlId)} presently")


        #Apped a last identifier if necessary
        if self.extraStrId is not "":
                mdlId = f"{mdlId}_{self.extraStrId}"
                 
        return mdlId

