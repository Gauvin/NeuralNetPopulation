

from keras.models import Model

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.manifold import TSNE
import numpy as np




MAX_ARRAY_SIZE = 10**4

class MdlLayerInspection(object):

    #Class attributes
    size = 15
    matplotlibParams = {'legend.fontsize': 'large',
              'figure.figsize': (20, 8),
              'axes.labelsize': size,
              'axes.titlesize': size,
              'xtick.labelsize': size * 0.75,
              'ytick.labelsize': size * 0.75,
              'axes.titlepad': 25}

    def __init__(self,
                 dataGeneratorFactory,
                 convNetParams,
                 pathBuilder,
                 mdlFit):


        self.dataGeneratorFactory = dataGeneratorFactory
        self.convNetParams =convNetParams
        self.pathBuilder = pathBuilder

        self.mdlId= mdlFit.mdlId

        #get the model
        self.convNetMdlCustom= mdlFit.convNetMdlCustom

        #Get the data generator
        self.dictTrainTestValid = dataGeneratorFactory.getTrainTestValidDataGenerators()


        #Save the results of the layer inspection for the different datasets(train,test,valid)
        self.dfFeatMapDict = {}
        self.dfTsneDict = {}



    def _getDfFeatureMap(self,dataGenerator,id):

        # Build the submodel - from input to the penultimate fuoly connected (affine + non-linear fct) layer
        fcLyr = self.convNetMdlCustom.get_layer('fc2')  # Consider the overall model, not the vgg feature extraction
        inputLyr = self.convNetMdlCustom.layers[0]
        subMdl = Model(inputLyr.input, fcLyr.output)

        # Get the last feature maps
        listPred = subMdl.predict(dataGenerator)  # Watchout!! this is the heavy part !!

        # Hack this shit
        # How the fuck can we have more predictions than observations? unless the iterator is looping somehow or some other weird edge case
        listPred = listPred[:, : len(self.dictTrainTestValid[id].y)]

        # Cram into a df where each column corresponds to an observation/imae
        dfFeatMap = pd.DataFrame(listPred.T)

        # Write the csv
        pathFeatureMap = self.pathBuilder.featureMapPath  / f'featureMaps_{self.mdlId}_{id}.csv'
        dfFeatMap.to_csv(str(pathFeatureMap))

        return dfFeatMap




    def plotTsneRepLastLayerAll(self):


        for k in self.dictTrainTestValid.keys():
            self.dfFeatMapDict[k] = self._getDfFeatureMap(self.dictTrainTestValid[k],k)

            #Get a 2D representation of the original high dimension (typically 16D) of the last activation map using Tsne
            tsneRep = TSNE().fit_transform(self.dfFeatMapDict[k].T)  # input should be n_obs X n_features
            dfTsne = pd.DataFrame(tsneRep)

            #Add in the response
            dfTsne['y'] = self.dictTrainTestValid[k].y

            #Add in the original observation index
            dfTsne['index'] = self.dictTrainTestValid[k].indices

            #Now merge on the initial dfGrid to add just about any variable
            dfGrid = self.dictTrainTestValid[k].dfGrid.reset_index()
            dfTsne = pd.merge(dfTsne, dfGrid, on='index')

            #Save
            self.dfTsneDict[k] = dfTsne

            #Plot the results for that layer by response and by neighbourhood
            self._plotTsneRepLastLayerSingleTrainTest(dfTsne, varToHighlight='y', extraStr='response' , dataSetID=k )
            self._plotTsneRepLastLayerSingleTrainTest(dfTsne ,varToHighlight='Q_socio', extraStr='neighbourhood' ,dataSetID=k)


        return True

    def _getDfFeatureMapFlatFc( self, dataGenerator, useFlat=False):

        # Build the submodel
        inputLyr = self.convNetMdlCustom.layers[0]
        if useFlat:
            fcLyr = self.convNetMdlCustom.get_layer('flatten')  # Consider the overall model, not the vgg feature extraction
        else:
            fcLyr = self.convNetMdlCustom.get_layer('fc2')  # Consider the overall model, not the vgg feature extraction

        subMdl = Model(inputLyr.input, fcLyr.output)

        # Get the last feature maps
        listPred = subMdl.predict(dataGenerator)

        # Cram into a df where each column corresponds to an observation/imae
        dfFeatMap = pd.DataFrame(listPred.T)

        #Quick check that the vector is not overly large and can fit into memory
        if dfFeatMap.shape[0] > MAX_ARRAY_SIZE :
            print(f"Array too large in mdlLayerInspection! Sampling up to {MAX_ARRAY_SIZE} elements")
            rndInd=np.random.choice( dfFeatMap.index, size=MAX_ARRAY_SIZE, replace=False)
            dfFeatMap = dfFeatMap.loc[ rndInd, ]


        return dfFeatMap


    def plotDistributionAllFCDim(self):
        self._plotDistributionAllFCWeights()
        self._plotDistributionAllFCDimWrappe(useFlat=True)
        self._plotDistributionAllFCDimWrappe(useFlat=False)


    def _getLayerWeights(self, layname):
        ''' Get the weights of a given layer

        :param layname:
        :return:
        '''
        X, b = self.convNetMdlCustom.get_layer(layname).get_weights()

        dfX = pd.DataFrame(X.ravel(), columns=['weight'])
        numParams = np.prod(X.shape)
        dfX['id'] = 'Linear part'

        dfB = pd.DataFrame(b.ravel(), columns=['weight'])
        numParams = np.prod(b.shape)
        dfB['id'] = 'Offset'

        dfBoth = pd.concat([dfX ,dfB])
        dfBoth['name'] = layname

        return dfBoth

    def _plotDistributionAllFCWeights(self):
        """Plot the distribution of weights for the last fc layer"""


        #Concat all 3 last layers
        listDf=[]
        listLayNames=['fc1', 'fc2', 'predictions']
        for l in listLayNames:
            listDf.append( self._getLayerWeights(l))

        dfBoth=pd.concat(listDf)
        dfBoth['id'] = dfBoth['id'].astype('category')
        dfBoth['id'] = dfBoth['id'].astype('category')

        g = sns.FacetGrid(col='id', row = 'name', hue='id', data=dfBoth)
        g.map(sns.violinplot, 'weight' )

        g.fig.suptitle('Last Fully connected layer weights', y=0.98,size=16)
        g.fig.tight_layout(rect=[0, 0.03, 1, 0.9])
        g.fig.set_figwidth(12)
        g.fig.set_figheight(7)

        g.savefig(   self.pathBuilder.figuresOutputPath / "LayerWeights" / f'weightFCCompare_{self.mdlId}.png')



    def _plotDistributionAllFCDimWrappe(self,useFlat = False):
        """Plot the output *feature maps* after either 1) the flatten layer or 2) the last fc layer

        """

        dictFeatMap = {}
        listDf = []

        for k in self.dictTrainTestValid.keys():
            dictFeatMap[k] = self._getDfFeatureMapFlatFc(dataGenerator=self.dictTrainTestValid[k], useFlat=useFlat)
            dfMelt = pd.melt(dictFeatMap[k])
            dfMelt['id'] = k
            listDf.append(dfMelt)

        listT = []
        for i in dictFeatMap.keys():
            dfT = dictFeatMap[i].T
            newColNames = {i: f'dim{i}' for i in dfT.columns} #change the col names to str for plotting
            dfT.rename(columns=newColNames, inplace=True)
            dfT['id'] = i
            listT.append(dfT)

        dfFeatMapAll = pd.concat(listT)
        dfFeatMapAll.reset_index(inplace=True)
        dfFeatMapAll['obs'] = range(dfFeatMapAll.shape[0])


        plt.figure(figsize=(10, 10))
        strName = 'Distribution of flatten layer after feature extractor' if useFlat else 'Distribution of penultimate 16D feature map'
        for r in range(4):
            for c in range(4):
                k = r + c * 4 + 1
                plt.subplot(4, 4, k)
                colNameDimStr = dfFeatMapAll.columns[k]
                ax = sns.violinplot(x='id', y=colNameDimStr, hue='id', data=dfFeatMapAll.loc[:, [colNameDimStr, 'id']],
                                    label='id')
                if k < 16:
                    ax.get_legend().set_visible(False)

        plt.suptitle(strName, y=0.95)
        plt.tight_layout(rect=[0, 0.03, 1, 0.85])

        plt.savefig( self.pathBuilder.figuresOutputPath / "FeatureMaps" / f"featMapDistri_useFlat_{useFlat}_{self.mdlId}.png")




    def _plotTsneRepLastLayerSingleTrainTest(self, dfTsne, varToHighlight, extraStr, dataSetID):

        plt.rcParams.update(MdlLayerInspection.matplotlibParams)

        fig = plt.figure(figsize=(10, 10))
        ax= sns.scatterplot(x=0, y=1, hue=varToHighlight, data=dfTsne)
        plt.legend(loc='upper left')
        ax.set_title(f"2D T-Sne representation from last fully connected layer\nBy {extraStr} - {dataSetID}")
        plt.tight_layout() #watch it, this needs to be at the end when everything has been plotted

        fig.savefig(self.pathBuilder.figuresOutputPath / "Tsne" / f"tsne_{dataSetID}_by_{extraStr}_{self.mdlId}.png")

        return True
