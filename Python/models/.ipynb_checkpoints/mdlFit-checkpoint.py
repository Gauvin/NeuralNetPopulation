

class MdlFit(object):
    
    def __init_(self,
                dataGeneratorFactory,
                convNetParams,
                pathBuilder,
                verbose=True
                ):
        
 
        self.dataGeneratorFactory = dataGeneratorFactory
        self.convNetParams=convNetParams
        self.pathBuilder = pathBuilder
        
        self.verbose =verbose

        
        
        
    def buildModel(self):
        '''Using the base feature extractor, 1-flatten 2- add a fc layer 3 (optional) add some regularization 4-set the final activation (depending on the response)
        
        Returns:
            convNetMdlCustom: tensorflow.python.keras.engine.training.Model
        
        '''
        #Instantiate the conv net (vgg, resnet, etc.)
        with warnings.catch_warnings () :
            convNetExtractor = self.convNetParams.convNetClass(include_top=False, 
                                       weights='imagenet',
                                       classes=1,
                                       input_shape= self.convNetParams.dim )
        if self.verbose:
            convNetExtractor.summary()
            
        
        #Fix the base layers-> use for feature extraction
        numLayers=len(convNetExtractor.layers)
        for k, layer in enumerate(convNetExtractor.layers):
            layer.trainable = False

            if k == numLayers-1:
                nameLast = layer.name

        inputTensorFeatExtractor = convNetExtractor.layers[0].input
        outputTensorFeatExtractor = convNetExtractor.layers(len(convNetExtractor.layers)-1).output 
        mdlFeatExtractor = Model(inputTensorFeatExtractor, outputTensorFeatExtractor)

        #Conv net/feature extractor output
        outputMdlSmaller=mdlFeatExtractor(inputTensor)

        #Flatten them + use these for prediction
        flatLyr = Flatten(name='flatten')(outputMdlSmaller)

        #Just one fully connected layers 
        #Try with L1 norm one and/or drop out if evidence of overfiting an poor generalsation power 
        finalDenseLyr = Dense(2**4, activation='relu', 
                              name='fc2' , 
                              activity_regularizer=l1(self.convNetParams.l1Coeff))(flatLyr) 
        
        #Can use aggressive dropout to avoid overfitting
        dropoutLyr = Dropout(rate=self.convNetParams.dropoutRate)(finalDenseLyr) 

        #Only 1 class: use sigmoid if resp is bounded within [0,1], else relu
        finalOutput = Dense(1, 
                            name='predictions',
                            activation=self.convNetParams.finalNonLin)(dropoutLyr) 

        #Final model
        convNetMdlCustom = Model(inputTensor, finalOutput )

        
        return convNetMdlCustom
    
    
    def fitMdl(self):
        
        
        mdlFile = self.pathBuilder.modelsOutputPath / f"model_{mdlId}.h5"
        mdlWeights = self.pathBuilder.modelsOutputPath / f"weights_{mdlId}.h5"
        historyPath = self.pathBuilder.historyOutputPath / f"history_{mdlId}.csv"

        allFileExits = [os.path.isfile(f) for f in [mdlFile,mdlWeights,historyPath ] ]

        if np.all(allFileExits) :

            convNetMdlCustom = keras.models.load_model(str(mdlFile)) 
            mdlHist = pd.read_csv(str(historyPath))

        else:
            
            #Get the data generators
            dictGenerators= self.dataGeneratorFactory.getTrainTestValidDataGenerators()

            mdlHist = convNetMdlCustom.fit_generator(generator=dictGenerators['train'],
                                               validation_data=dictGenerators['valid'])
                                               epochs=self.convNetParams.numEpochs,
                                               batch_size=self.convNetParams.batchSize,
                                               shuffle=self.convNetParams.shuffle,
                                               callbacks=[es1],
                                               verbose=1)

            #convert the dictionary to a dataframe to standardize behaviour
            mdlHist= pd.DataFrame(mdlHist.history)
            mdlHist.to_csv(str(historyPath))


            # Save the model - save both weights AND model
            convNetMdlCustom.save(str(mdlFile))
            convNetMdlCustom.save_weights(str( mdlWeights))