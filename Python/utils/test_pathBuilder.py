import unittest
import inspect
import numpy as np
import sys

import os
home = os.environ['HOME']
sys.path.append(f"{home}/Projects/NeuralNetPopulation/Python")

from utils.pathBuilder import PathBuilder

rootDir = f'{home}/Projects/NeuralNetPopulation'
provider = 'Mapbox'
gridSize = 100
resp="v_CA16_406"

class TestPathBuilder(unittest.TestCase):


    def test_path_attributes(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        #Get all public attributes from the instance
        listAllMembers = inspect.getmembers(myPathBuilder, lambda x: not (inspect.isroutine(x)))
        listAttr = [a[0] for a in listAllMembers if not (a[0].startswith('__') and a[0].endswith('__'))]

        #Make sure all attributes have valid values
        isNone = np.all([attr is not None for attr in listAttr])
        assert isNone


if __name__ == '__main__':
    unittest.main()