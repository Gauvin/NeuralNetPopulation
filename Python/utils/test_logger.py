import unittest
import sys

from keras.applications.vgg19 import VGG19
from sklearn.preprocessing import MinMaxScaler

import os
home = os.environ['HOME']
sys.path.append(f"{home}/Projects/NeuralNetPopulation/Python")


from dataGeneration.dataGenerator import DataGenerator
from dataGeneration.rawDataInput import RawDataInput

from models.convNetParams import ConvNetParams

from utils.pathBuilder import PathBuilder
from utils.logger import Logger

rootDir = f'{home}/Projects/NeuralNetPopulation'
provider = 'Mapbox'
gridSize = 100
resp="v_CA16_406"
# final non-linearity: sigmoid to predict [0,1] normalized value vs relu otherwise (since response is non-negative)
finalNonLin = "sigmoid" if resp == "v_CA16_2540" else "relu"


class TestLogger(unittest.TestCase):


    def test_logger(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      resp=resp,
                                      provider=provider,
                                      gridSize=gridSize)





        myConvNetParams= ConvNetParams(  provider=provider,
                                         gridSize=gridSize,
                                         resp=myRawDataInput.resp,
                                         dim=myRawDataInput.dim,
                                         numImgs= myRawDataInput.numImgs,
                                         finalNonLin=finalNonLin,
                                         lr = 5*10**-4,
                                         numEpochs=20 ,
                                         batchSize=2**5 ,
                                         l1Coeff = 0 ,
                                         dropoutRate=0.1,
                                         convNetClass = VGG19,
                                         scalerClass =MinMaxScaler,
                                         shuffle=False,
                                         verbose=True )

        myLogger = Logger( myPathBuilder,
                myConvNetParams)

        myLogger.logger.warning("Test")

