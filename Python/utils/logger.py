
from datetime import date
import logging

class Logger(object):


    def __init__(self,
                 pathBuilder,
                 convNetParams):

        self.pathBuilder = pathBuilder
        self.convNetParams = convNetParams
        self.logger = self._getLogger()




    def _getLogger(self):

        logger = logging.getLogger( self.convNetParams.convNetName )
        hdlr = logging.FileHandler(self.pathBuilder.loggerPath / f"log_{date.today()}_{self.convNetParams.mdlId}.txt")
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

        logger.addHandler(hdlr)
        logger.warning(f"{date.today()}")

        return logger