

from pathlib import Path
import os


class PathBuilder(object):
    
    
    def __init__(self,
                 rootDir,
                 provider,
                 gridSize):
        
        self.rootDir = rootDir
        self.provider = provider
        self.gridSize= gridSize

        self.predictionErrorPath = self._getPredictionErrorPath()
        self.partitionPath = self._getPartitionIndicesDataPath()
        self.figuresOutputPath = self._getOutputFigurePath()
        self.modelsOutputPath = self._getModelPath()
        self.historyOutputPath= self._getHistoryPath()
        self.shpFilePath = self._getShpCensusPath()
        self.shpNeigh = self._getShpNeigh()
        self.pngPath = self._getPngPath()
        self.csvPath = self._getCsvPath()
        self.loggerPath =self._getLoggerPath()
        self.checkPointPath = self._getCheckPointPath()
        self.featureMapPath = self._getFeatureMapPath()
        self.picklePath =self._getPicklePath()

    def _getPicklePath(self):
        picklePath = Path(self.rootDir) /"Data" / "Models" / 'Pickle'/ self.provider / str(self.gridSize)
        self._createDirIfNotExisting(picklePath)
        return picklePath

    def _getPredictionErrorPath(self):

        # Path to out put figures
        predictionErrorPath = Path(self.rootDir) /"Data" / "Models" / 'PredictionError'/ self.provider / str(self.gridSize)
        self._createDirIfNotExisting(predictionErrorPath)
        return predictionErrorPath

    def _getPartitionIndicesDataPath(self):

        # Path to out put figures
        partitionPath = Path(self.rootDir) /"Data" / "Models" / 'Partition'/ self.provider / str(self.gridSize)
        self._createDirIfNotExisting(partitionPath)

        return partitionPath

    def _getOutputFigurePath(self):
        
        #Path to out put figures
        figuresOutputPath = Path(self.rootDir) / "Figures" / self.provider / str(self.gridSize)
        self._createDirIfNotExisting(figuresOutputPath)
        return figuresOutputPath

    def _getFeatureMapPath(self):

        # Path to output tmp models
        featureMapPath = Path(self.rootDir) / "Data" / "Models" / 'FeatureMaps' / self.provider / str(self.gridSize)
        self._createDirIfNotExisting(featureMapPath)
        return featureMapPath

    def _getModelPath(self):
        
        #Path to output tmp models
        modelsOutputPath = Path(self.rootDir)/ "Data" / "Models" / 'FinalModel' / self.provider / str(self.gridSize )
        self._createDirIfNotExisting(modelsOutputPath)
        return modelsOutputPath
            
            
    def _getHistoryPath(self):
        
        #Path to save the history as a csv
        historyOutputPath = Path(self.rootDir)/ "Data" / "Models" /  "ModelHistory" / self.provider / str(self.gridSize )
        self._createDirIfNotExisting(historyOutputPath)
        return historyOutputPath


    def _getCheckPointPath(self):

        checkpointPath = Path(self.rootDir) / "Data" / "Models" / 'Checkpoint' / self.provider / str(self.gridSize)
        self._createDirIfNotExisting(checkpointPath)
        return checkpointPath
            
    def _getShpCensusPath(self):
        
        #Path to census shp file
        shpFilePath = Path(self.rootDir) /  "Data" / "GeoData" / "Shp" / "shpCensusMtl.shp"
        return shpFilePath

    def _getShpNeigh(self):

        # Path to census shp file
        shpNeigh = Path(self.rootDir) / "Data" / "GeoData" / "Shp" / "Quartiers_sociologiques_2014.shp"
        return shpNeigh


    def _getLoggerPath(self):
        logBasePath= Path(self.rootDir) / "Log" / "mdlFittingLogs" / str(self.provider) / str(self.gridSize)
        self._createDirIfNotExisting(logBasePath)
        return logBasePath


    def _getPngPath(self):
        pathPng=Path(self.rootDir) /"Output" / str(self.provider) / str(self.gridSize) / "Images"
        return pathPng
    
    def _getCsvPath(self):
        pathCsv = Path(self.rootDir) /"Output" / str(self.provider) / str(self.gridSize) / "Csv"
        return pathCsv
        


    def _createDirIfNotExisting(self, dirPath):

        if not os.path.exists(dirPath):
            print(f"Creating directory {dirPath}")
            os.makedirs(dirPath)
        else:
            print(f"Directory {dirPath} already exists")

        return True