

class PathBuilder(object):
    
    
    def __init__(self,
                 rootDir,
                 provider,
                 gridSize):
        
        self.rootDir = rootDir
        self.provider = provider
        self.gridSize= gridSize
        
        self.figuresOutputPath = self._getOutputFigurePath()
        self.modelsOutputPath = self._getModelPath()
        self.historyOutputPath= self._getHistoryPath()
        self.shpFilePath = self._getShpCensusPath()
        self.pngPath = self._getPngPath
        self.csvPath = self._getCsvPath()
        self.loggerPath =self._getLoggerPath()
        
        
        
    def _getOutputFigurePath(self):
        
        #Path to out put figures
        figuresOutputPath = Path(self.rootDir) / "Figures" / self.provider / str(self.gridSize)

        if not os.path.exists(figuresOutputPath):
            print(f"Creating directory {figuresOutputPath}")
            os.makedirs(figuresOutputPath)
        else:
            print(f"Directory {figuresOutputPath} already exists")
            
        return figuresOutputPath
                        
    def _getModelPath(self):
        
        #Path to output tmp models
        modelsOutputPath = Path(self.rootDir)/ "Data" / "Models" / self.provider / str(self.gridSize )

        if not os.path.exists(modelsOutputPath):
            print(f"Creating directory {modelsOutputPath}")
            os.makedirs(modelsOutputPath)
        else:
            print(f"Directory {modelsOutputPath} already exists")
            
        return modelsOutputPath
            
            
    def _getHistoryPath(self):
        
        #Path to save the history as a csv
        historyOutputPath = Path(self.rootDir) / "Data" / "History" / self.provider / str(self.gridSize )

        if not os.path.exists(historyOutputPath):
            print(f"Creating directory {historyOutputPath}")
            os.makedirs(historyOutputPath)
        else:
            print(f"Directory {historyOutputPath} already exists")
            
        return historyOutputPath
            
    def _getShpCensusPath(self):
        
        #Path to census shp file
        shpFilePath = Path(self.rootDir) /  "Data" / "GeoData" / "Shp" / "shpCensusMtl.shp"
        return shpFilePath
    
    
    def _getPngPath(self):
        pathPng=Path(self.rootDir) /"Output" / str(self.provider) / str(self.gridSize) / "Images"
        return pathPng
    
    def _getCsvPath(self):
        pathCsv = Path(self.rootDir) /"Output" / str(self.provider) / str(self.gridSize) / "Csv"
        return pathCsv
        
    def _getLoggerPath(self):
        logBasePath= Path(self.rootDir) / "Log" / str(self.provider) / str(self.gridSize) 
        return logBasePath