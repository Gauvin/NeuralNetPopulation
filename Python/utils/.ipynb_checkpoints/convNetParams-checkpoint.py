

class ConvNetParams(object):
    
    
    def __init__(self,
                 provider,
                 gridSize,
                 convNetName,
                 dim,
                 finalNonLin,
                 lr = 5*10**-4,
                 numEpochs=20 ,
                 batchSize=2**5 ,
                 l1Coeff = 0 ,
                 dropoutRate=0.1,
                 convNetClass = VGG19,
                 shuffle=False,
                 verbose=True):

            self.provider = provider
            self.gridSize = gridSize
            self.convNetNameconvNetName = convNetName
            self.dim=dim
            self.finalNonLin = finalNonLin
            
            self.lr =lr
            self.numEpochs=numEpochs
            self.batchSize=batchSize
            self.l1Coeff = l1Coeff
            self.dropoutRate=dropoutRate
            self.convNetClass = convNetClass
            self.shuffle = shuffle
            self.verbose=verbose
            
            self.mdlId = self._getMdlName()
            
            
            
    def _getMdlName(self):
        
        mdlId = f"{self.provider}_{self.resp}_{self.convNetName}_" \
                f"gridSize_{self.gridSize}_lr_{self.lr}_batchSize_{self.batchSize}_" \
                f"numEpochs_{self.numEpochs}_l1Coeff_{self.l1Coeff}_dropoutRate_{self.dropoutRate}"
                 
        return mdlId
