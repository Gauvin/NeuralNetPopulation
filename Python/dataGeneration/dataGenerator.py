import numpy as np
from PIL import Image
from keras.utils import Sequence

import warnings
import logging



class DataGenerator(Sequence):
    """ Read images from disk on the fly
    
    Adapted from https://stanford.edu/os.environ["HOME"]shervine/blog/keras-how-to-generate-data-on-the-fly 
      
    """  
    def __init__(self,
                 dfGrid,
                 indices,
                 resp,
                 dim,
                 batch_size,
                 scaler,
                 imgScaleFactor=255.0,
                 shuffle=True):
        
        self.dfGrid = dfGrid
        self.dim= dim
        self.resp =resp
        
        self.batch_size= min(len(indices), batch_size)
        if self.batch_size == len(indices):
            print(f"The number of indices must be at least as large as the batch size! Cannot consider a batch of size {self.batch_size} with only {len(indices)} items")
        
        self.scaler = scaler
        self.shuffle = shuffle
        self.indices = indices

        self.imgScaleFactor = imgScaleFactor
        print(f"Using a scale factor of {imgScaleFactor} in DataGenerator")


        if self.shuffle :
            np.random.shuffle(self.indices) #works in place

        #If we want to access the raw data later on, this should keep track of the shufled values
        self.y= self.dfGrid.loc[self.indices, self.resp ]
        self.yScaled = scaler.transform( self.dfGrid.loc[self.indices, self.resp].values.reshape(-1,1) )
            
    def __len__(self):
        return int(np.ceil(len(self.indices)/self.batch_size))
    
    
    
    def __getitem__(self,index):
        
        if self.batch_size*index >= len(self.indices):
            raise Exception(f"Array dimension error! can only consider index of up to {self.__len__()}")
        
        indices = self.indices [ index * self.batch_size:(index+1)*self.batch_size]
        
        
        X, y = self.__data_generation(indices)
        
        return X,y
    
 
            
    def __data_generation(self,indices):
        
        X = np.concatenate( [self.readImgAsArray(p) for p in self.dfGrid.loc[indices, 'pathImg'] ] )
        y = self.scaler.transform( self.dfGrid.loc[indices, self.resp ].values.reshape(-1,1) )
           
        return X, y
        
        
    def readImgAsArray(self, path):
        
        im =Image.open(path)
        imTensorToReturn = np.expand_dims(np.array(im)/self.imgScaleFactor, 0)
        im.close() #don't forget to close the image, otherwise this will blow up


        return imTensorToReturn