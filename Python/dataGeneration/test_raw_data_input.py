
import unittest
import inspect
import numpy as np
import sys

import os
home = os.environ['HOME']
sys.path.append(f"{home}/Projects/NeuralNetPopulation/Python")

from dataGeneration.rawDataInput import RawDataInput
from utils.pathBuilder import PathBuilder

rootDir = f'{home}/Projects/NeuralNetPopulation'
provider = 'Mapbox'
gridSize = 100
resp="v_CA16_406"

class TestRawData(unittest.TestCase):


    def test_raw_data(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)


        myRawDataInput = RawDataInput(myPathBuilder,
                                      provider=provider,
                                      gridSize=gridSize,
                                      idGrid='gridId',
                                      resp=resp)

        assert myRawDataInput.dfGrid is not None
        assert myRawDataInput.listPngs is not None
        assert myRawDataInput.dim is not None

        assert myRawDataInput.dim == (480,480,3)