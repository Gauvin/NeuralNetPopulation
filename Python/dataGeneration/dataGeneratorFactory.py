
from pathlib import Path
import numpy as np
from PIL import Image
import math
import sys
import os
from psutil import virtual_memory, cpu_percent, swap_memory
from sklearn.preprocessing import MinMaxScaler

import os
home = os.environ['HOME']
sys.path.append(f"{home}/Projects/NeuralNetPopulation/Python")

from dataGeneration.dataGenerator import DataGenerator
from dataGeneration.rawDataInput import RawDataInput
from utils.logger import Logger



class DataGeneratorFactory(object):
    
    '''Class to generate the train, test and valid data generators
    
    
    '''
    
    def __init__(self,
                 rawDataInput,
                 convNetParams,
                 pathBuilder):
        
        self.rawDataInput = rawDataInput
        self.convNetParams = convNetParams

        self.scaler = convNetParams.scalerClass()
        self.batchSize = convNetParams.batchSize
        self.dim= convNetParams.dim

        myLogger = Logger(pathBuilder, convNetParams)
        self.logger =  myLogger.logger
        self.pathBuilder=pathBuilder

        self.propTrain = convNetParams.propTrain
        self.propVal= convNetParams.propVal

        
        self.idxTrain=None
        self.idxTest=None
        self.idxValid=None

        self.yTrainMean = None
        self.yTrainMed = None

        self.benchmarkConstantPredMean=None
        self.benchmarkConstantPredMed = None

        self._initDataFactory() #build the train,test valid split and compute the train mean and median

    
    def _getValidIdx(self, sliceIdx):

        ''' Get the valid observations given a slice/subset of observations

        params:
            sliceIdx : slide t
        returns:
            tuple of (np.array of indices, np.arrray of iamges - which all have the same shape)
        '''

        idxList=[]
        listIm=[]
        for  f in self.rawDataInput.listPngs[sliceIdx] :

            #Read the image
            im= Image.open(f)

            if im.mode != "RGB":
                im = im.convert("RGB")

            #Convert to array
            array=np.array(im)
            picId = (f.stem)
            picIdx=np.where( self.rawDataInput.dfGrid[self.rawDataInput.idGrid] == picId )[0]
            if len(picIdx) > 1 :
                raise Exception(f"Fatal error! picture {picId} appears more than once ({len(picIdx)} times) in the grid shp (0 times is possible)")

            im.close()

            #Get the response 
            dfFiltered = self.rawDataInput.dfGrid.loc[ self.rawDataInput.dfGrid[self.rawDataInput.idGrid] == picId, self.rawDataInput.resp]
            nrow = dfFiltered.shape[0]

            #Make sure we have all 3 rgb channels + the response is correct
            if nrow == 1 and len(array.shape) == 3 and ~np.all(np.isnan(dfFiltered)) :
                idxList.append(picIdx)
                listIm.append(array)
            else:
                srtrDim = ",".join( [str(s) for s in array.shape] )
                self.logger.warning(f"Skipping file {f.stem} - the dimensions do not match {srtrDim} - number of matches :{nrow} - resp is nan? {dfFiltered}")


        return np.array(idxList).astype(int)






    def _validateIndexPartition(self, idxTrain, idxTest, idxValid):

        ''' Make sure we really partition the indices into three disjoint non-empty subsets 

        '''
        if len(idxValid) <= 0 or len(idxTrain) <=0 or len(idxTest) <= 0 : 
            raise Exception("Fatal error! need at least 1 observation for both the train, valid and test sets")



        #Quick validation: test and train sets must be disjoint  3! = 6 different checks
        assert( ~np.any(np.isin( idxTrain , idxTest)))
        assert( ~np.any(np.isin( idxTrain , idxValid)))

        assert( ~np.any(np.isin( idxTest  , idxTrain)))
        assert( ~np.any(np.isin( idxTest  , idxValid)))

        assert( ~np.any(np.isin( idxValid , idxTrain)))
        assert( ~np.any(np.isin( idxValid , idxTest)))

        # make sure we do not send in multidimensional indices
        self.idxTrain = idxTrain.ravel()
        self.idxTest = idxTest.ravel()
        self.idxValid = idxValid.ravel()

        return True



    def _createTrainTestValid(self):

        ''' Given a set of RGB images (png) and the associated response somewhere in dfGrid[resp], 
             split the features and responses into train, test and validation sets

        '''

        numTotal = self.rawDataInput.numImgs
        numTrain= math.floor(numTotal*self.propTrain) 
        numTest= numTotal - numTrain
 


        idxTrain= self._getValidIdx( slice(0,numTrain) )
        idxTest= self._getValidIdx( slice(numTrain,numTotal))

        #Now the validation test from the train
        np.random.shuffle(idxTrain)
        numValid=int(np.ceil(self.propVal*len(idxTrain)))
        idxValid = idxTrain[:numValid]
        idxTrain = idxTrain[ ~np.isin(idxTrain,idxValid) ]




        return idxTrain , idxTest,  idxValid



    def _initDataFactory(self):

        ''' Function to call at the instance creation

        Guarantees that the indices can be queryed + mean and med of train resp can be obtained safely

        :return:
        '''

        # Check if the indices already exist, if not save for later on so that we make
        pathTrain = Path(self.pathBuilder.partitionPath / f"train_{self.convNetParams.mdlId}.npy")
        pathTest = Path(self.pathBuilder.partitionPath / f"test_{self.convNetParams.mdlId}.npy")
        pathValid = Path(self.pathBuilder.partitionPath / f"valid_{self.convNetParams.mdlId}.npy")

        allFileExits = [os.path.isfile(f) for f in [pathTrain, pathTest, pathValid]]

        if np.all(allFileExits):
            print(f"All train, test, valid indices already exist for {self.convNetParams.mdlId}")
            idxTrain, idxTest, idxValid = np.load(pathTrain), np.load(pathTest), np.load(pathValid)
            # Validate the partition
            self._validateIndexPartition(idxTrain, idxTest, idxValid)
        else:
            # Get the train,test and valid indices
            print("Creating new train, test and valid indices")
            idxTrain, idxTest, idxValid = self._createTrainTestValid()
            # Validate the partition
            self._validateIndexPartition(idxTrain, idxTest, idxValid)

            [np.save(pathIdx, idx) for idx, pathIdx in
             zip([idxTrain, idxTest, idxValid], [pathTrain, pathTest, pathValid])]

        # Fit the scaler on the TRAIN dataset only
        # Reshape the response to a column vector
        respVect = self.rawDataInput.dfGrid.loc[idxTrain, self.rawDataInput.resp].values.reshape(-1, 1)

        # Fit the scaler
        yFit = self.scaler.fit_transform(respVect)

        # Save the mean and median (possibly for initialization later)
        self.yTrainMed = np.median(yFit)
        self.yTrainMean = np.mean(yFit)

        self.benchmarkConstantPredMed = np.sum(np.abs(yFit-self.yTrainMed))/yFit.shape[0]
        self.benchmarkConstantPredMean = np.std(yFit) **2

        if self.benchmarkConstantPredMed < self.benchmarkConstantPredMean :
            raise Exception("Fatal error initializing benchmarks in dataGeneratorFactory: {self.benchmarkConstantPredMed} < {self.benchmarkConstantPredMean} (MAD > MSE for inputs in [0,1])")

        # Quick check that normalization is working
        if len(set(yFit.ravel())) == 1:
            print(f"Warning! there is a single distinct element and {len(yFit)} elements in the partition")
        else:
            if (type(self.scaler) == MinMaxScaler and (
                    np.isclose(np.min(yFit), 0) == False or np.isclose(np.max(yFit), 1) == False)):
                raise Exception(
                    f"Fatal error! scaled value with MinMaxScaler should be in 0,1 -- min: {np.min(yFit)} - max: {np.max(yFit)}")




    def getTrainTestValidDataGenerators(self):
        ''' Main entry point: Create a dictionnary of 3 data generators: test, train and validation
        
        
        Returns:
            dictGen : dictionary with keys "train",'test','valid' and values given by DataGenerator()
        '''


        #Create the 3 data generators
        dictGen = {name:  DataGenerator(dfGrid = self.rawDataInput.dfGrid,
                                         indices= idx, 
                                         resp=self.rawDataInput.resp,
                                         dim=self.rawDataInput.dim,
                                         batch_size=self.convNetParams.batchSize,
                                         scaler=self.scaler) \
                   for idx, name in zip( [self.idxTrain,self.idxTest, self.idxValid], ["train",'test','valid'])
                  }

        #Chek ram usage
        self.logger.warning( f'Swap memory: {swap_memory()}\n')
        self.logger.warning(f'Virtual memory: {virtual_memory()}\n')
        #Check cpu usage
        self.logger.warning(f'CPU usage: {cpu_percent(interval=None)}\n')

        return dictGen