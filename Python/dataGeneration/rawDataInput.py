

from PIL import Image
import numpy as np
import geopandas as gpd
import pandas as pd
from pathlib import Path

class RawDataInput(object):
    
    
    def __init__(self,
                 pathBuilder,
                 resp,
                 provider,
                 gridSize,
                 idGrid='gridId',
                 ubNumTotal = np.inf):
        
        self.pathBuilder = pathBuilder
        self.ubNumTotal = max(1, ubNumTotal)

        self.idGrid = idGrid
        self.dfGrid = self._getDfGrid()
        self.listPngs = self._getListPngs()
        self.numImgs = len(self.listPngs)
        self.dim = self._getDim()
        self.resp=resp
        self.provider=provider
        self.gridSize=gridSize
        self.shpCensus = self._getShpCensus()
        self.shpNeigh = self._getShpNeigh()

    def _getListPngs (self):

        # watch out for possible duplicates
        listPngs= np.unique([ f for f in self.pathBuilder.pngPath.glob("*.png")] )
        numImgs = min(self.ubNumTotal, len(listPngs))

        return listPngs[:numImgs]
        
        
    def _getDfGrid (self):

        #read the csv
        dfGrid=pd.read_csv(self.pathBuilder.csvPath  / "dfGrid.csv")

        #Check presence of idgrid
        if self.idGrid not in dfGrid.columns:
            raise   Exception(f"Fatal error in RawDataInput._getDfGrid() => {self.idGrid} is not in the columns of dfGrid")

        # remove duplicate images
        dfGrid.drop_duplicates(self.idGrid, inplace=True)

        #Add the full absolute image path
        pngPathPathObj=Path(self.pathBuilder.pngPath)
        dfGrid['pathImg'] = [ str( pngPathPathObj / f"{r}.png") for r in dfGrid[self.idGrid]]
    
        return dfGrid

    def _getShpCensus(self):

        # Read in the census vars + gemometry
        shpCensus = gpd.read_file(self.pathBuilder.shpFilePath)
        shpCensus['GeoUID'] = shpCensus['GeoUID'].astype(str).astype('int64')

        return shpCensus

    def _getShpNeigh(self):

        # Read in the census vars + gemometry
        shpNeigh = gpd.read_file(self.pathBuilder.shpNeigh)
        shpNeigh= shpNeigh.to_crs(self._getShpCensus().crs) #make sure the crs match

        return shpNeigh


    def _getDim(self):
        
        #Get the dimension (fixed) for all images
        imRep = Image.open( self.dfGrid.loc[1, "pathImg"] )
        dim= np.array(imRep).shape
        
        return dim