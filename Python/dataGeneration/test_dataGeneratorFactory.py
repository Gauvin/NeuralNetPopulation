
import unittest
import inspect
import numpy as np
import sys
import math
from keras.applications.vgg19 import VGG19
from sklearn.preprocessing import MinMaxScaler
import os

import os
home = os.environ['HOME']
sys.path.append(f"{home}/Projects/NeuralNetPopulation/Python")

from dataGeneration.dataGenerator import DataGenerator
from dataGeneration.dataGeneratorFactory import DataGeneratorFactory
from dataGeneration.rawDataInput import RawDataInput

from models.convNetParams import ConvNetParams
from utils.pathBuilder import PathBuilder


rootDir = f'{home}/Projects/NeuralNetPopulation'
provider = 'Mapbox'
gridSize = 100
resp="v_CA16_406"
# final non-linearity: sigmoid to predict [0,1] normalized value vs relu otherwise (since response is non-negative)
finalNonLin = "sigmoid" if resp == "v_CA16_2540" else "relu"



class TestDataGeneratorFactory(unittest.TestCase):


    def test_train_valid_indices_small(self):


        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(  myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal =10    ) #consider a subset of the data

        myConvNetParams= ConvNetParams(  provider=provider,
                                         gridSize=gridSize,
                                         resp=myRawDataInput.resp,
                                         dim=myRawDataInput.dim,
                                         numImgs= myRawDataInput.numImgs,
                                         finalNonLin=finalNonLin,
                                         lr = 5*10**-4,
                                         numEpochs=20 ,
                                         batchSize=2**5 ,
                                         l1Coeff = 0 ,
                                         dropoutRate=0.1,
                                         convNetClass = VGG19,
                                         scalerClass =MinMaxScaler,
                                         shuffle=False,
                                         verbose=True )



        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput ,
                                                      myConvNetParams,
                                                      myPathBuilder)

        tupleArrayIndices = myDatageneratorFactory._createTrainTestValid()


        assert len(tupleArrayIndices) ==3







    def test_all(self):

        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      idGrid='gridId',
                                      provider=provider,
                                      gridSize=gridSize,
                                      resp=resp,
                                      ubNumTotal=10)  # consider a subset of the data

        myConvNetParams = ConvNetParams(provider=provider,
                                        gridSize=gridSize,
                                        resp=myRawDataInput.resp,
                                        dim=myRawDataInput.dim,
                                        numImgs=myRawDataInput.numImgs,
                                        finalNonLin=finalNonLin,
                                        lr=5 * 10 ** -4,
                                        numEpochs=20,
                                        batchSize=2 ** 5,
                                        l1Coeff=0,
                                        dropoutRate=0.1,
                                        convNetClass=VGG19,
                                        scalerClass=MinMaxScaler,
                                        shuffle=False,
                                        verbose=True)

        myDatageneratorFactory = DataGeneratorFactory(myRawDataInput,
                                                      myConvNetParams,
                                                      myPathBuilder)

        dictTrainTestValid = myDatageneratorFactory.getTrainTestValidDataGenerators()

        assert myDatageneratorFactory.idxTrain is not None
        assert myDatageneratorFactory.idxTest is not None
        assert myDatageneratorFactory.idxValid is not None

        # Train, test valid
        assert len(dictTrainTestValid.keys()) == 3

        #Make sure the data is normalized
        for k in dictTrainTestValid.keys() :

            if np.isin( k, ['train']): #scaler computed on the train/valid only

                for it in dictTrainTestValid[k] : #dictTrainTestValid[k] returns an iterator
                    X, y = it
                    assert np.min(y) >= 0 and np.max(y) <= 1




if __name__ == '__main__':
    unittest.main()