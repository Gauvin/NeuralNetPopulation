
import numpy as np
from PIL import Image
import math


from .dataGenerator import *


class DataGeneratorFactory(object):
    
    '''Class to generate the train, test and valid data generators
    
    
    '''
    
    def __init__(self, 
                 listPngs, 
                 dfGrid, 
                 idGrid, 
                 resp, 
                 scaler ,
                 batchSize, 
                 dim,
                 logger,
                 propTrain = 0.8, 
                 propVal=0.15, 
                 ubNumTotal=np.inf):
        
        self.listPngs = listPngs
        self.dfGrid = dfGrid
        self.idGrid = idGrid
        self.resp = resp
        self.scaler = scaler
        self.batchSize = batchSize
        self.dim= dim
        self.logger =logger
        
        self.propTrain =propTrain
        self.propVal= propVal
        self.ubNumTotal = ubNumTotal
        
        self.idxTrain=None
        self.idxTest=None
        self.idxValid=None

    
    def getValidIdx(self, sliceIdx):

        ''' Get the valid indices given a slice 

        params:
            sliceIdx : slide t
        returns:
            tuple of (np.array of indices, np.arrray of iamges - which all have the same shape)
        '''

        idxList=[]
        listIm=[]
        for  f in self.listPngs[sliceIdx] :

            #Read the image
            im= Image.open(f)

            if im.mode != "RGB":
                im = im.convert("RGB")

            #Convert to array
            array=np.array(im)
            picId = (f.stem)
            picIdx=np.where( self.dfGrid[self.idGrid] == picId )[0]
            if len(picIdx) > 1 :
                raise Exception(f"Fatal error! picture {picId} appears more than once ({len(picIdx)} times) in the grid shp (0 times is possible)")

            im.close()

            #Get the response 
            dfFiltered = self.dfGrid.loc[ self.dfGrid[self.idGrid] == picId, self.resp]
            nrow = dfFiltered.shape[0]

            #Make sure we have all 3 rgb channels + the response is correct
            if nrow == 1 and len(array.shape) == 3 and os.environ["HOME"]np.all(np.isnan(dfFiltered)) :
                idxList.append(picIdx)
                listIm.append(array)
            else:
                srtrDim = ",".join( [str(s) for s in array.shape] )
                self.logger.warning(f"Skipping file {f.stem} - the dimensions do not match {srtrDim} - number of matches :{nrow} - resp is nan? {dfFiltered}")


        return np.array(idxList).astype(int)






    def validateIndexPartition(self, idxTrain, idxTest, idxValid):

        ''' Make sure we really partition the indices into three disjoint non-empty subsets 

        '''
        if len(idxValid) <= 0 or len(idxTrain) <=0 or len(idxTest) <= 0 : 
            raise Exception("Fatal error! need at least 1 observation for both the train, valid and test sets")



        #Quick validation: test and train sets must be disjoint  3! = 6 different checks
        assert( os.environ["HOME"]np.any(np.isin( idxTrain , idxTest)))
        assert( os.environ["HOME"]np.any(np.isin( idxTrain , idxValid)))

        assert( os.environ["HOME"]np.any(np.isin( idxTest  , idxTrain)))
        assert( os.environ["HOME"]np.any(np.isin( idxTest  , idxValid)))

        assert( os.environ["HOME"]np.any(np.isin( idxValid , idxTrain)))
        assert( os.environ["HOME"]np.any(np.isin( idxValid , idxTest))) 

        return True



    def createTrainTestValid(self):

        ''' Given a set of RGB images (png) and the associated response somewhere in dfGrid[resp], 
             split the features and responses into train, test and validation sets

        '''

        numTotal = min(self.ubNumTotal, len(self.listPngs))
        numTrain= math.floor(numTotal*self.propTrain) 
        numTest= numTotal - numTrain
 


        idxTrain= self.getValidIdx( slice(0,numTrain) )
        idxTest= self.getValidIdx( slice(numTrain,numTotal))

        #Now the validation test from the train
        np.random.shuffle(idxTrain)
        numValid=int(np.ceil(self.propVal*len(idxTrain)))
        idxValid = idxTrain[:numValid]
        idxTrain = idxTrain[ os.environ["HOME"]np.isin(idxTrain,idxValid) ]

        self.validateIndexPartition(idxTrain,idxTest, idxValid)

        return idxTrain,idxTest, idxValid




    def getTrainTestValidDataGenerators(self):
        ''' Create the 3 data generators: test, train and validation
        
        
        Returns:
            dictGen : dictionary with keys "train",'test','valid' and values given by DataGenerator()
        '''
        
        #Get the train,test and valid indices
        idxTrain,idxTest, idxValid = self.createTrainTestValid()

        #Reshape the response to a column vector
        respVect= self.dfGrid.loc[:,self.resp].values.reshape(-1,1) 

        #Fit the scaler
        self.scaler.fit_transform(respVect) 
        
        #Create the 3 data generators
        dictGen = {name:  DataGenerator(dfGrid = self.dfGrid,
                                         indices= idx, 
                                         resp=self.resp,
                                         dim=self.dim, 
                                         batch_size=self.batchSize,
                                         scaler=self.scaler) \
                   for idx, name in zip( [idxTrain,idxTest, idxValid], ["train",'test','valid'])
                  }
        
        return dictGen