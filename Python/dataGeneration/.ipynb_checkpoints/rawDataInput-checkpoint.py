

from PIL import Image
from pathlib import Path
import numpy as np
import glob



class(object):
    
    
    def __init__(self,
                 pathBuilder):
        
        self.pathBuilder = pathBuilder
        
        
        self.dfGrid = self._getDfGrid()
        self.listPngs = self._getListPngs()
        self.dim = self._getDim()
        
    
    def _getListPngs (self):
        
        pathPng=Path(rootDir) /"Output" / str(provider) / str(gridSize) / "Images"
        pathCsv = Path(rootDir) /"Output" / str(provider) / str(gridSize) / "Csv"
        listPngs= np.unique([ f for f in pathPng.glob("*.png")] ) # watch out for possible duplicates
        mdlId=f"{mdlId}_numImgs_{len(listPngs)}"

    
        return listPngs
        
        
    def _getDfGrid (self):
        
        dfGrid=pd.read_csv(pathCsv / "dfGrid.csv")
        dfGrid.drop_duplicates(idGrid, inplace=True) #remove duplicate images
        dfGrid['pathImg'] = [ str(Path(pathPng) / f"{r}.png") for r in dfGrid[idGrid]]
    
        return dfGrid


    def _getDim(self):
        
        #Get the dimension (fixed) for all images
        imRep = Image.open( self.dfGrid.loc[1, "pathImg"] )
        dim= np.array(imRep).shape
        
        return dim