
import unittest
import inspect
import numpy as np
import sys
import math
from keras.applications.vgg19 import VGG19
from sklearn.preprocessing import MinMaxScaler

import os
home = os.environ['HOME']
sys.path.append(f"{home}/Projects/NeuralNetPopulation/Python")

from dataGeneration.dataGenerator import DataGenerator
from dataGeneration.rawDataInput import RawDataInput

from models.convNetParams import ConvNetParams
from utils.pathBuilder import PathBuilder


rootDir = f'{home}/Projects/NeuralNetPopulation'
provider = 'Mapbox'
gridSize = 100
resp="v_CA16_406"

class TestDataGenerator(unittest.TestCase):


    def test_train_generator(self):


        myPathBuilder = PathBuilder(rootDir,
                                    provider,
                                    gridSize)

        myRawDataInput = RawDataInput(myPathBuilder,
                                      resp = resp,
                                      provider=provider,
                                      gridSize =gridSize,
                                      idGrid='gridId')

        # final non-linearity: sigmoid to predict [0,1] normalized value vs relu otherwise (since response is non-negative)
        finalNonLin = "sigmoid" if resp == "v_CA16_2540" else "relu"


        myConvNetParams= ConvNetParams(  provider=myRawDataInput.provider,
                                         gridSize=myRawDataInput.gridSize,
                                         resp=myRawDataInput.resp,
                                         dim=myRawDataInput.dim,
                                         numImgs= myRawDataInput.numImgs,
                                         finalNonLin=finalNonLin,
                                         lr = 5*10**-4,
                                         numEpochs=20 ,
                                         batchSize=2**5 ,
                                         l1Coeff = 0 ,
                                         dropoutRate=0.1,
                                         convNetClass = VGG19,
                                         scalerClass =MinMaxScaler,
                                         shuffle=False,
                                         verbose=True )

        trainSize=6
        indicesTrain=myRawDataInput.dfGrid.index[:trainSize].values
        scaler = myConvNetParams.scalerClass()
        scaler.fit(myRawDataInput.dfGrid[resp].values.reshape(-1,1))

        myDatagenerator = DataGenerator(dfGrid = myRawDataInput.dfGrid,
                                         indices= indicesTrain,
                                         resp=myConvNetParams.resp,
                                         dim=myRawDataInput.dim,
                                         batch_size=myConvNetParams.batchSize,
                                         scaler=scaler)

        assert len(myDatagenerator) == math.ceil(len(myRawDataInput.dfGrid.index[:trainSize].values)/myConvNetParams.batchSize)

        for k in range(len(myDatagenerator)):
            X,y=myDatagenerator[k]

            assert np.min(y) >= 0 and np.max(y) <= 1


if __name__ == '__main__':
    unittest.main()