

writeImages <- function(shpGridInterpolated,
                        rast,
                        gridSize,
                        idCol="gridId",
                        provider="Mapbox",
                        naThreshMax=0.1){
  tic()
  num <- nrow(shpGridInterpolated)
  idxSuccess <- map_lgl(1:num , 
                       ~ cropMasRastkBySf(shpGridInterpolated[.x,],
                                          rast = rast,
                                          gridSize= gridSize, 
                                          idCol = idCol,
                                          provider=provider,
                                          naThreshMax=naThreshMax))
 
  successProp <- sum(idxSuccess)/num

  toc(log = T)
  timerStringResults <- tic.log(format=T)[[1]]
  tic.clearlog()
  
  #Write the time taken to run
  strMsg <- glue("Total time for cropping and masking: {timerStringResults}\nProporion of successs: {successProp}")
  cat(strMsg, '\n',
      file = getPriofileLogPath() ,
      append = TRUE)
  
  print(glue("Proporion of successs:{successProp}"))
  
  return(idxSuccess)
}





writeCsv <-  function(shpGridInterpolated,
                     gridSize,
                     provider="Mapbox"){
  
  idRast <- unique(shpGridInterpolated$rastID)
  
  stopifnot(length(idRast)<=1)
  if(length(idRast) == 0) {print("No csv written -> 0 rows")}
  else{
    df <- shpGridInterpolated %>% st_set_geometry(NULL)
    write_csv(x = df, 
              path = here("Output",provider, gridSize , "Csv", glue("dfGrid_{idRast}.csv")))
  }
  

  return(T)
}






writeImagesCsv <-  function(shpGridInterpolated,
                           rast,
                           gridSize,
                           idCol="gridId",
                           provider="Mapbox",
                           naThreshMax=0.1){
  
  
  
  #Create the drirectories ifnecessary
  GeneralHelpers::createDirIfNotExistentRecursive( here("Output", provider, gridSize))
  GeneralHelpers::createDirIfNotExistentRecursive( here("Output", provider, gridSize, "Images")) 
  GeneralHelpers::createDirIfNotExistentRecursive( here("Output", provider, gridSize, "Csv")) 
  
  #Get the final csv file and write if required
  idRast <- unique(shpGridInterpolated$rastID)
  pathCsvFinal <- here("Output",provider, gridSize , "Csv", glue("dfGrid_{idRast}.csv"))
  
  imgSuccess <- T
  csvSuccess <- T
  
  if(!file.exists(pathCsvFinal)){
    #Write the images
    idxSuccess <- writeImages (shpGridInterpolated,
                               rast,
                               gridSize,
                               idCol="gridId",
                               provider=provider,
                               naThreshMax =naThreshMax)
    imgSuccess <- sum(idxSuccess) > 0
    
    #Write the csv only if the img was successfully cropped and masked
    csvSuccess <- writeCsv(shpGridInterpolated[idxSuccess, ],
                           gridSize,
                           provider=provider)
    
    
  }else{
    print(glue("Not considering raster{idRast} -> aleady done"))
  }

  #Bind all the csvs into one
  listCsvsPaths <- list.files( here("Output",provider, gridSize , "Csv"), pattern = ".csv$",full.names = T)
  listCsvsPaths <- listCsvsPaths[ listCsvsPaths != "dfGrid.csv"] #do not consider the preexisting file
  listCsvs <- map(listCsvsPaths, read_csv )
  dfBinded <- do.call(rbind,listCsvs)
  write_csv(dfBinded,  
            here("Output",provider, gridSize , "Csv", "dfGrid.csv"),
            append = F) #after each raster, rewrite the entire thing cumulatively
  
  #Check success and print
  if(imgSuccess & csvSuccess){
    print("Success")
    ret <- T
  }else{
    print("Failure when writing csv and images")
    ret <- T
  }
  
  return(ret)
}