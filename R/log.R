


getPriofileLogPath <- function(){
  
  here::here('Log',
             'profileLogs' , 
             glue('{str_replace_all(string=lubridate::today(),pattern = "[ :-]", replacement = "_")}log.txt')
  )
  
}


 

#' Log the time taken to run a function and save he text results to a log file
#'
#' @param f
#'
#' @return
#' @export
#'
#' @examples With some funtion f: fTimed <- logFctCallWithTime( f , getPriofileLogPath() )
#' then fTimed(...)
logFctCallWithTime <- function(f, file) {
  
  
  function (...) {
    
    require(tictoc)
    
    # Write the function call
    cat(deparse(match.call()), '\n',
        file = file,
        append = TRUE)
    
    tic()
    
    returnVal = f(...)
    
    toc(log=T)
    timerStringResults <- tic.log(format=T)[[1]] #this returns a list with one character
    
    
    #Write the time taken to run
    cat(timerStringResults, '\n',
        file = file,
        append = TRUE)
    
    tic.clearlog()
    
    #Also write the memory used
    cat(glue::glue("Memory used: {pryr::mem_used()}\n=======\n"), '\n',
        file = file,
        append = TRUE)
    
    return(returnVal)
    
  }
}


